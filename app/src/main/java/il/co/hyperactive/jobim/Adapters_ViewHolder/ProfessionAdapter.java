package il.co.hyperactive.jobim.Adapters_ViewHolder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

import il.co.hyperactive.jobim.Model.Profession;
import il.co.hyperactive.jobim.R;

/**
 * Created by Tom on 24/05/2017.
 */
public class ProfessionAdapter extends RecyclerView.Adapter<ProfessionAdapter.ProfessionHolder> {

    private List<Profession> mProfessions;
    private Context context;
    public int mCheckedPosition=-1;
    public ProfessionAdapter(List<Profession> professions,Context context) {
        mProfessions = professions;
        this.context=context;
    }

    @Override
    public ProfessionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.line_content_profession, parent, false);
        return new ProfessionHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProfessionHolder holder, int position) {
        Profession profession = mProfessions.get(position);
        holder.bindProfession(profession);
        holder.radioButton.setOnCheckedChangeListener(null);
        holder.radioButton.setChecked(position == mCheckedPosition);
        holder.radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCheckedPosition = holder.getAdapterPosition();
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mProfessions.size();
    }
    public class ProfessionHolder extends RecyclerView.ViewHolder{

        RadioButton radioButton;
        TextView textView;
        ImageView imageView;

        public ProfessionHolder(View itemView) {
            super(itemView);
            radioButton=(RadioButton)itemView.findViewById(R.id.line_content_profession_radioButton);
            textView=(TextView)itemView.findViewById(R.id.line_content_profession_textView);
            imageView=(ImageView)itemView.findViewById(R.id.line_content_profession_imageView);
        }

        public void bindProfession(Profession profession) {
            textView.setText(profession.getString_id_textView());
            imageView.setImageResource(profession.getResuorce_id_imageView());
        }
    }
}
