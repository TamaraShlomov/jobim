package il.co.hyperactive.jobim.Fragments.createNewJob;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import il.co.hyperactive.jobim.Fragments.registration.RegistrationInputFragment;
import il.co.hyperactive.jobim.R;

public class JobDescriptionFragment extends RegistrationInputFragment {
    EditText et_job_description_title,et_more_desctiption;
    public JobDescriptionFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         View rootView= inflater.inflate(R.layout.fragment_job_description, container, false);
         et_job_description_title=(EditText)rootView.findViewById(R.id.job_description_et_job_title);
         et_more_desctiption=(EditText)rootView.findViewById(R.id.job_description_et_more_description);

         return rootView;
    }

    @Override
    public String getParams() {
        String result="";
        if(et_job_description_title.equals("")) {
            result = "error";
            et_job_description_title.setBackgroundResource(R.drawable.edit_text_user_input_error);
            Toast.makeText(getActivity(),"אנא בחר כתובת לג'וב",Toast.LENGTH_SHORT).show();
        }
        else {
            result = et_job_description_title.getText().toString() + "," + et_more_desctiption.getText().toString();
            et_job_description_title.setBackgroundResource(R.drawable.edit_text_user_input);
        }
        return result;
    }
}
