package il.co.hyperactive.jobim.Fragments.createNewJob;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import il.co.hyperactive.jobim.Fragments.registration.RegistrationInputFragment;
import il.co.hyperactive.jobim.Model.CompanyLab;
import il.co.hyperactive.jobim.R;

public class JobCompanyFragment extends RegistrationInputFragment {
    EditText et_branch;
    AutoCompleteTextView auto_complete_tv_company_name;
    List<String> compniesName;
    public JobCompanyFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_job_company, container, false);
        auto_complete_tv_company_name=(AutoCompleteTextView) rootView.findViewById(R.id.job_company_auto_complete_tv_company_name);
        et_branch=(EditText)rootView.findViewById(R.id.job_company_et_branch_name);
        compniesName=CompanyLab.getInstance().getCompanies();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),android.R.layout.simple_list_item_1,compniesName);
        auto_complete_tv_company_name.setAdapter(adapter);
        auto_complete_tv_company_name.setThreshold(1);
        hideKeyBoardInFragment();
        return rootView;
    }

    @Override
    public String getParams() {
        String selectedCompany= auto_complete_tv_company_name.getText().toString();
        Log.i("tom","item selected "+selectedCompany);
        if(!selectedCompany.equals("") && !compniesName.contains(selectedCompany))
            CompanyLab.getInstance().addCompany(selectedCompany);

        String result=selectedCompany+","+et_branch.getText().toString();
        if(selectedCompany.equals("")){
            result="error";
            auto_complete_tv_company_name.setBackground(getResources().getDrawable(R.drawable.edit_text_user_input_error));
            Toast.makeText(getActivity(),"חובה למלא את שם החברה",Toast.LENGTH_SHORT).show();
        }
        else
            auto_complete_tv_company_name.setBackground(getResources().getDrawable(R.drawable.edit_text_user_input));
        return result;
    }
}
