package il.co.hyperactive.jobim.Model;

import android.support.annotation.ColorRes;
import android.support.v7.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import il.co.hyperactive.jobim.R;

/**
 * Created by Tom on 23/05/2017.
 */

public class CompanyLab {
    private static CompanyLab CompanyLab;
    private List<String> companiesName;
    private CompanyLab(){
        companiesName=new ArrayList<>();
        Collections.addAll(companiesName,"סופר פארם","סלקום","הוט","ביטוח ישיר","דיוטי פרי","1800flowers",
                "דגים 206","am:pm");
    }

    public static CompanyLab getInstance(){
        if(CompanyLab==null)
            CompanyLab=new CompanyLab();
        return CompanyLab;
    }
    public List<String> getCompanies(){
        return companiesName;
    }

    public void addCompany(String companyName){
        companiesName.add(companyName);
    }
}
