package il.co.hyperactive.jobim.Adapters_ViewHolder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

import il.co.hyperactive.jobim.Model.JobimLocation;
import il.co.hyperactive.jobim.Model.NotificationSettings;
import il.co.hyperactive.jobim.R;

/**
 * Created by Tom on 24/05/2017.
 */
public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.LocationHolder> {

    private List<String> addresses;
    private Context context;
    public int mCheckedPosition=-1;
    public LocationAdapter(List<String> addresses,Context context) {
        this.addresses = addresses;
        this.context=context;
    }
    public List<String> getAddresses(){
        return addresses;
    }

    @Override
    public LocationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.line_content_location, parent, false);
        return new LocationHolder(view);
    }

    @Override
    public void onBindViewHolder(final LocationHolder holder, int position) {
        String address = addresses.get(position);
        holder.bindLocation(address);
        holder.radioButton.setOnCheckedChangeListener(null);
        holder.radioButton.setChecked(position == mCheckedPosition);
        holder.radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCheckedPosition = holder.getAdapterPosition();
                //todo: check if this working right 4.7.17
                NotificationSettings.getInstance().
                        setSelectedLocation(JobimLocation.getInstance(context).
                                getLocationLatLngFromAddress( getAddresses().get(mCheckedPosition))
                       );
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return addresses.size();
    }

    public class LocationHolder extends RecyclerView.ViewHolder{

        RadioButton radioButton;
        TextView textView;

        public LocationHolder(View itemView) {
            super(itemView);
            radioButton=(RadioButton)itemView.findViewById(R.id.line_content_location_radioButton);
            textView=(TextView)itemView.findViewById(R.id.line_content_location_textView);
        }

        public void bindLocation(String address) {
            textView.setText(address);
        }
    }
}
