package il.co.hyperactive.jobim.Fragments.createNewJob;


import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kishan.askpermission.AskPermission;
import com.kishan.askpermission.ErrorCallback;
import com.kishan.askpermission.PermissionCallback;
import com.kishan.askpermission.PermissionInterface;

import java.io.File;

import il.co.hyperactive.jobim.Activitys.FiltetQuestionActivity;
import il.co.hyperactive.jobim.Fragments.registration.RegistrationInputFragment;
import il.co.hyperactive.jobim.R;

public class JobOptionalDetailsFragment extends RegistrationInputFragment implements PermissionCallback, ErrorCallback, View.OnClickListener {
    ImageView imageView_job_image;
    CheckBox checkBox_sutable_for_teens;
    TextView textView_filter_question;
    Uri imagePath;
    private static final int TAKE_PICTURE = 1;
    private final int MY_PERMISSIONS_REQUEST_USE_CAMERA=1;
    private final int REQUEST_CODE_FILTER_QUESTION=2;
    String filter_question_and_answer;
    AlertDialog alertDialog;
    private View.OnClickListener onClickAlertDialogButton;
    private static final int SELECT_IMAGE = 3;

    public JobOptionalDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_job_optional_details, container, false);
        imageView_job_image=(ImageView)rootView.findViewById(R.id.job_optional_details_imageView);
        checkBox_sutable_for_teens=(CheckBox) rootView.findViewById(R.id.job_optional_details_checkBox);
        textView_filter_question=(TextView) rootView.findViewById(R.id.job_optional_details_tv_adding_filter_q);

        textView_filter_question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getContext(),FiltetQuestionActivity.class);
                startActivityForResult(intent,REQUEST_CODE_FILTER_QUESTION);
            }
        });

        imageView_job_image.setOnClickListener(this);
        onClickAlertDialogButton = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickAlertDialogButton(v);
            }
        };
        return rootView;
    }
    //---------------------------take_picture- start-----------------------------//
    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = new File(Environment.getExternalStorageDirectory(),"jobPic.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
        if(Build.VERSION.SDK_INT>23) {
            imagePath = FileProvider.getUriForFile(getContext(), getContext().getApplicationContext().getPackageName() + ".provider", photo);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            ClipData clip= ClipData.newUri(getActivity().getContentResolver(), "A photo", imagePath);
            intent.setClipData(clip);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        }else
            imagePath = Uri.fromFile(photo);
        Log.e("image uri",imagePath.toString());
        startActivityForResult(intent, TAKE_PICTURE);
    }
    @Override
    public void onPermissionsGranted(int requestCode) {
        Toast.makeText(getActivity(), "Permissions Received.", Toast.LENGTH_LONG).show();
        takePhoto();
    }
    @Override
    public void onPermissionsDenied(int requestCode) {
        Toast.makeText(getActivity(), "Permissions Denied.", Toast.LENGTH_LONG).show();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_USE_CAMERA:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("permission granted","permission granted");
                } else
                    Log.e("permission denied","permission denied");

                break;
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TAKE_PICTURE:
                if (resultCode == Activity.RESULT_OK) {
                    if (imagePath != null) {
                       // imageView_job_image.setImageURI(null);
                       // imageView_job_image.setImageURI(imagePath);
                        imageView_job_image.setBackground(getImageDrawable());
                    }
                }
                break;
            case REQUEST_CODE_FILTER_QUESTION:
                if (resultCode == Activity.RESULT_OK) {
                    Log.e("data",data.toString());
                    Bundle result=data.getBundleExtra("result");
                    filter_question_and_answer=result.getCharSequence("q").toString();
                    filter_question_and_answer+=","+result.getCharSequence("answer").toString();
                }
                break;
            case SELECT_IMAGE:
                if(resultCode == Activity.RESULT_OK){
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = JobOptionalDetailsFragment.this.getActivity().
                            getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String filePath = cursor.getString(columnIndex);
                    cursor.close();


                    Bitmap mySelectedImage = BitmapFactory.decodeFile(filePath);
                    Drawable d = new BitmapDrawable(getResources(), mySelectedImage);
                    imageView_job_image.setImageDrawable(d);
                 //   imageView_job_image.setImageBitmap(mySelectedImage);
                    imagePath = selectedImage;
                }
                break;
        }
    }

    public Drawable getImageDrawable(){
        Drawable imageDrawable=null;
        getActivity().getContentResolver().notifyChange(imagePath, null);
        ContentResolver cr = getActivity().getContentResolver();

        try {
            Bitmap bitmap = android.provider.MediaStore.Images.Media
                    .getBitmap(cr, imagePath);
            imageDrawable=new BitmapDrawable(getResources(),bitmap);
            Toast.makeText(getActivity(), imagePath.toString(), Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Failed to load", Toast.LENGTH_SHORT)
                    .show();
            Log.e("Camera", e.toString());
        }
        return imageDrawable;
    }

    @Override
    public void onShowRationalDialog(PermissionInterface permissionInterface, int requestCode) {}

    @Override
    public void onShowSettings(PermissionInterface permissionInterface, int requestCode) {}
    //---------------------------take_picture- end-----------------------------//
    @Override
    public String getParams() {
        String result="";
        if(imagePath!=null)
            result+=imagePath.toString()+",";
        else
            result+="empty"+",";
        result+=checkBox_sutable_for_teens.isChecked();
        if(filter_question_and_answer!=null)
            result+=","+filter_question_and_answer;
        else
            result+=","+"empty";
        return result;
    }

    public void createAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View mView= getActivity().getLayoutInflater().inflate(R.layout.custom_alert_dialog,null);
        TextView title,message;
        Button btn_right_gallery,btn_left_camera;
        title=(TextView)mView.findViewById(R.id.custom_alert_dialog_title);
        message=(TextView)mView.findViewById(R.id.custom_alert_dialog_message);
        btn_left_camera=(Button)mView.findViewById(R.id.custom_alert_dialog_btn_left);
        btn_right_gallery=(Button)mView.findViewById(R.id.custom_alert_dialog_btn_right);

        title.setText("");
        message.setText(R.string.message_picture);
        btn_left_camera.setText(R.string.camera);
        btn_right_gallery.setText(R.string.gallery);

        btn_left_camera.setOnClickListener(onClickAlertDialogButton);
        btn_right_gallery.setOnClickListener(onClickAlertDialogButton);
        alertDialogBuilder.setView(mView);
        alertDialog=alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();
    }

    public void onClickAlertDialogButton(View view) {
        switch (view.getId()){
            case R.id.custom_alert_dialog_btn_right:
                Log.i("tom","gallery");
                openGallery();
                break;
            case R.id.custom_alert_dialog_btn_left:
                Log.i("tom","camera");
                //  requestPermission();
                new AskPermission.Builder(JobOptionalDetailsFragment.this)
                        .setPermissions(Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .setCallback(JobOptionalDetailsFragment.this)
                        .setErrorCallback(JobOptionalDetailsFragment.this)
                        .request(TAKE_PICTURE);
                break;
            default:
                Log.i("tom","default");
                break;
        }
        alertDialog.cancel();
    }

    public void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECT_IMAGE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.job_optional_details_imageView:
                createAlertDialog();
            break;
        }
    }
}
