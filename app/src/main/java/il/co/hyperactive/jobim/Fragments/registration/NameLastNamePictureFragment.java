package il.co.hyperactive.jobim.Fragments.registration;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kishan.askpermission.AskPermission;
import com.kishan.askpermission.ErrorCallback;
import com.kishan.askpermission.PermissionCallback;
import com.kishan.askpermission.PermissionInterface;

import java.io.File;


import de.hdodenhof.circleimageview.CircleImageView;
import il.co.hyperactive.jobim.Model.SharedPreferencesSingleton;
import il.co.hyperactive.jobim.Model.User;
import il.co.hyperactive.jobim.Model.Validation;
import il.co.hyperactive.jobim.R;

import static android.support.v4.content.ContextCompat.checkSelfPermission;

/**
 * Created by Tom on 08/05/2017.
 */

public class NameLastNamePictureFragment extends RegistrationInputFragment implements PermissionCallback, ErrorCallback, View.OnClickListener {
    private static final int SELECT_IMAGE = 2;
    final String Tag="NameLastName";
    public static final String FRAGMENT_NAME="NameLastNamePictureFragment";
    CircleImageView iv_profile_picture;
    EditText et_first_name,et_last_name;
    private static final int TAKE_PICTURE = 1;
    Uri imagePath;
    public static Bitmap bitmap;
    AlertDialog alertDialog;
    private View.OnClickListener onClickAlertDialogButton;
    public NameLastNamePictureFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        hideKeyBoardInFragment();
        View rootView = inflater.inflate(R.layout.fragment_name_lastname_picture, container, false);
        iv_profile_picture=(CircleImageView) rootView.findViewById(R.id.profile_image);
        et_first_name=(EditText)rootView.findViewById(R.id.editText_name);
        et_last_name=(EditText)rootView.findViewById(R.id.editText_lastname);

        iv_profile_picture.setOnClickListener(this);

        if(SharedPreferencesSingleton.getInstance(getActivity()).readUser()!=null){
            User user=SharedPreferencesSingleton.getInstance(getActivity()).readUser();
            et_first_name.setText(user.name);
            et_last_name.setText(user.lastname);
            if(!user.photoPath.equals("empty")) {
                imagePath=Uri.parse(user.photoPath);
                iv_profile_picture.setImageDrawable(getImageDrawable());
            }

        }

        onClickAlertDialogButton = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickAlertDialogButton(v);
            }
        };
        return rootView;
    }

    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = new File(Environment.getExternalStorageDirectory(),"profilePic.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));

        if(Build.VERSION.SDK_INT>23) {
            imagePath = FileProvider.getUriForFile(getContext(), getContext().getApplicationContext().getPackageName() + ".provider", photo);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            ClipData clip= ClipData.newUri(getActivity().getContentResolver(), "A photo", imagePath);
            intent.setClipData(clip);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        }else
         imagePath = Uri.fromFile(photo);

        Log.e("image uri",imagePath.toString());
        startActivityForResult(intent, TAKE_PICTURE);
    }

    @Override
    public void onPermissionsGranted(int requestCode) {
        Toast.makeText(getActivity(), "Permissions Received.", Toast.LENGTH_LONG).show();
        takePhoto();
    }
    @Override
    public void onPermissionsDenied(int requestCode) {
        Toast.makeText(getActivity(), "Permissions Denied.", Toast.LENGTH_LONG).show();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case TAKE_PICTURE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("permission granted","permission granted");
                } else
                    Log.e("permission denied","permission denied");

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TAKE_PICTURE:
                if (resultCode == Activity.RESULT_OK) {
                  //  Bitmap bitmap=getBitmap();
                    Drawable bitmapDrawable=getImageDrawable();
                    if(bitmapDrawable!=null) {
                        //iv_profile_picture.setImageBitmap(bitmap);
                        iv_profile_picture.setImageDrawable(bitmapDrawable);
                    }
                }
                break;
            case SELECT_IMAGE:
                if(resultCode == Activity.RESULT_OK){
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getActivity().getContentResolver().
                            query(selectedImage, filePathColumn, null, null, null);
                    if (cursor != null) {
                        cursor.moveToFirst();
                    }

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String filePath = cursor.getString(columnIndex);
                    cursor.close();


                    Bitmap mySelectedImage = BitmapFactory.decodeFile(filePath);
                    Drawable d = new BitmapDrawable(getResources(), mySelectedImage);
                    iv_profile_picture.setImageDrawable(d);
                    imagePath = selectedImage;

                }
                break;
        }
    }

    public Bitmap getBitmap(){
        Bitmap bitmap=null;
        getActivity().getContentResolver().notifyChange(imagePath, null);
        ContentResolver cr = getActivity().getContentResolver();

        try {
            bitmap = android.provider.MediaStore.Images.Media
                    .getBitmap(cr, imagePath);
            Toast.makeText(getActivity(), imagePath.toString(), Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Failed to load", Toast.LENGTH_SHORT)
                    .show();
            Log.e("Camera", e.toString());
        }
        return bitmap;
    }

    public Drawable getImageDrawable(){
        Drawable imageDrawable=null;
        getActivity().getContentResolver().notifyChange(imagePath, null);
        ContentResolver cr = getActivity().getContentResolver();

        try {
            Bitmap bitmap = android.provider.MediaStore.Images.Media
                    .getBitmap(cr, imagePath);
            imageDrawable=new BitmapDrawable(getResources(),bitmap);
            Toast.makeText(getActivity(), imagePath.toString(), Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Failed to load", Toast.LENGTH_SHORT)
                    .show();
            Log.e("Camera", e.toString());
        }
        return imageDrawable;
    }

    @Override
    public void onShowRationalDialog(PermissionInterface permissionInterface, int requestCode) {
    }
    @Override
    public void onShowSettings(PermissionInterface permissionInterface, int requestCode) {
    }
    private void requestPermission() {
        if (checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA))
                Toast.makeText(getActivity(), "permission allows us to access . Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
            else
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, TAKE_PICTURE);
        }
        else
            takePhoto();
    }

    @Override
    public String getParams() {
        String imagePath_string=imagePath==null?"empty":imagePath.toString();
       String result= et_first_name.getText().toString()+","+ et_last_name.getText().toString()+","+imagePath_string;
       if(!Validation.isValidField(Tag, et_first_name.getText().toString())) {
           et_first_name.setBackground(getResources().getDrawable(R.drawable.edit_text_user_input_error));
           result="error";
       }
        else
           et_first_name.setBackground(getResources().getDrawable(R.drawable.edit_text_user_input));

        if(!Validation.isValidField(Tag, et_last_name.getText().toString())) {
            et_last_name.setBackground(getResources().getDrawable(R.drawable.edit_text_user_input_error));
            result= "error";
        }
        else
            et_last_name.setBackground(getResources().getDrawable(R.drawable.edit_text_user_input));

        return result;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.profile_image:
               createAlertDialog();
                break;
        }
    }

    public void createAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View mView= getActivity().getLayoutInflater().inflate(R.layout.custom_alert_dialog,null);
        TextView title,message;
        Button btn_right_gallery,btn_left_camera;
        title=(TextView)mView.findViewById(R.id.custom_alert_dialog_title);
        message=(TextView)mView.findViewById(R.id.custom_alert_dialog_message);
        btn_left_camera=(Button)mView.findViewById(R.id.custom_alert_dialog_btn_left);
        btn_right_gallery=(Button)mView.findViewById(R.id.custom_alert_dialog_btn_right);

        title.setText("");
        message.setText(R.string.message_picture);
        btn_left_camera.setText(R.string.camera);
        btn_right_gallery.setText(R.string.gallery);

        btn_left_camera.setOnClickListener(onClickAlertDialogButton);
        btn_right_gallery.setOnClickListener(onClickAlertDialogButton);
        alertDialogBuilder.setView(mView);
        alertDialog=alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();
    }

    public void onClickAlertDialogButton(View view) {
        switch (view.getId()){
            case R.id.custom_alert_dialog_btn_right:
                Log.i("tom","gallery");
                openGallery();
                break;
            case R.id.custom_alert_dialog_btn_left:
                Log.i("tom","camera");
                //  requestPermission();
                new AskPermission.Builder(NameLastNamePictureFragment.this)
                        .setPermissions(Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .setCallback(NameLastNamePictureFragment.this)
                        .setErrorCallback(NameLastNamePictureFragment.this)
                        .request(TAKE_PICTURE);
                break;
            default:
                Log.i("tom","default");
                break;
        }
        alertDialog.cancel();
    }

    public void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECT_IMAGE);
    }
}
