package il.co.hyperactive.jobim.Fragments.searchJobs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompatSideChannelService;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import il.co.hyperactive.jobim.Adapters_ViewHolder.ProfessionAdapter;
import il.co.hyperactive.jobim.Adapters_ViewHolder.ProfessionMultiChoiseAdapter;
import il.co.hyperactive.jobim.Fragments.createNewJob.JobProfessionFragment;
import il.co.hyperactive.jobim.Model.NotificationSettings;
import il.co.hyperactive.jobim.Model.Profession;
import il.co.hyperactive.jobim.Model.ProfessionLab;
import il.co.hyperactive.jobim.Model.SearchJobSingleton;
import il.co.hyperactive.jobim.R;

public class JobProfessionMultiChoiseFragment extends Fragment implements TextWatcher {
    View rootview;
    private RecyclerView mProfessionRecyclerView;
    private List<Profession> professions;
    private ProfessionMultiChoiseAdapter mAdapter;
    EditText search_job_edit_text;
    ProfessionLab professionLab;
    SearchJobSingleton searchJobSingleton;
    List<Integer> selectedPositions;
    boolean searchJob;
    private NotificationSettings notificationSettings;

    public JobProfessionMultiChoiseFragment() {
        // Required empty public constructor
    }
    public static Fragment newInstance(boolean searchJob){
        Bundle bundle=new Bundle();
        bundle.putBoolean("searchJob",searchJob);
        JobProfessionMultiChoiseFragment fragment = new JobProfessionMultiChoiseFragment();
        fragment.setArguments(bundle);
        return fragment;
    }
    public void readBundle(Bundle bundle){
        if(bundle!=null)
            searchJob=bundle.getBoolean("searchJob");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        readBundle(getArguments());
        if(searchJob) {
            searchJobSingleton = SearchJobSingleton.getInstance();
            selectedPositions = searchJobSingleton.getSelectedJobs();
        }
        else{
            notificationSettings=NotificationSettings.getInstance();
            selectedPositions=notificationSettings.getSelectedJobs();
        }
        rootview= inflater.inflate(R.layout.fragment_job_profession_multi_choise, container, false);

        professionLab = ProfessionLab.getInstance();
        professions = professionLab.getProfessions();

        mProfessionRecyclerView = (RecyclerView) rootview.findViewById(R.id.profession_recycler_view);
        mProfessionRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                mProfessionRecyclerView.getContext(), LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.line_seperator_gray));
        mProfessionRecyclerView.addItemDecoration(dividerItemDecoration);
        updateUI();
        search_job_edit_text=(EditText)rootview.findViewById(R.id.jon_profession_et_search);
        search_job_edit_text.addTextChangedListener(this);
        return rootview;

    }

    private void updateUI() {
        mAdapter = new ProfessionMultiChoiseAdapter(professions,getActivity(),selectedPositions,searchJob);
        mProfessionRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    public List<Integer> getSelectedJobs(){
        List<Integer> selectedJobs=new ArrayList<>();
        if(mAdapter!=null) {
             List<Boolean> mCheckBoxStates=mAdapter.mCheckBoxStates;
            for(int i=0;i<mCheckBoxStates.size();i++)
                if(mCheckBoxStates.get(i))
                    selectedJobs.add(i);
        }
        return selectedJobs;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if(s.equals(""))
            professions = professionLab.getProfessions();
        else {
            List<Profession> temp = new ArrayList<>();
            for (Profession profession : professions) {
                String prof = getString(profession.getString_id_textView());
                if (prof.contains(s))
                    temp.add(profession);
            }
            professions=temp;
        }
        updateUI();
    }

    @Override
    public void afterTextChanged(Editable s) {}

    @Override
    public void onPause() {
        if (searchJob)
            searchJobSingleton.setSelectedJobs(getSelectedJobs()); //נראה לי מיותר
        else
            notificationSettings.setSelectedJobs(getSelectedJobs());//נראה לי מיותר
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
