package il.co.hyperactive.jobim.Fragments.createNewJob;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import il.co.hyperactive.jobim.Adapters_ViewHolder.LocationAdapter;
import il.co.hyperactive.jobim.Fragments.registration.RegistrationInputFragment;
import il.co.hyperactive.jobim.Interface.AsyncResponse;
import il.co.hyperactive.jobim.Model.JobimLocation;
import il.co.hyperactive.jobim.Model.NotificationSettings;
import il.co.hyperactive.jobim.R;
import il.co.hyperactive.jobim.Tasks.AdressTask;

public class JobLocationFragment extends RegistrationInputFragment
        implements AsyncResponse {
    private RecyclerView mLocationRecyclerView;
    private EditText et_adress;
    private LocationAdapter mAdapter;
    private TextView title;
    private  boolean choose_location;
    public JobLocationFragment() {
        // Required empty public constructor
    }
    public static RegistrationInputFragment newInstance(boolean choose_location){
        Bundle bundle = new Bundle();
        bundle.putBoolean("choose_location",choose_location);

        JobLocationFragment fragment = new JobLocationFragment();
        fragment.setArguments(bundle);
        return fragment;
    }
    private void readBundle(Bundle bundle) {
        if (bundle != null)
            choose_location =bundle.getBoolean("choose_location");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_job_location, container, false);
        title=(TextView)rootview.findViewById(R.id.job_location_tv) ;
        readBundle(getArguments());

        mLocationRecyclerView = (RecyclerView) rootview.findViewById(R.id.job_location_recyclerView);
        mLocationRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        et_adress = (EditText) rootview.findViewById(R.id.job_location_et_address);
        et_adress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String alldigits = "0123456789";
                String[] result = s.toString().split(" ");
                if (result != null && result.length >= 2)
                    if (result[0].length() >= 2) {
                        for (int i = 0; i < result[1].length(); i++)
                            if (!alldigits.contains(result[1].charAt(i) + ""))
                                break;
                        AdressTask adressTask = new AdressTask(getActivity(), JobLocationFragment.this);
                        adressTask.execute(s.toString());
                    }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        boolean needFindCurrentLocation=false;
        if(choose_location) {
            changeTitle(R.string.choose_location);
            final LatLng latlng=NotificationSettings.getInstance().getSelectedLocation();
            if(latlng!=null) {
                String location= JobimLocation.getInstance(getActivity()).getAddressByLatLng(latlng);
                et_adress.setText(location);
                List<String> list=new ArrayList<>();
                list.add(location);
                mAdapter = new LocationAdapter(list, getActivity());
                mLocationRecyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();// not sure if this line is necessary
            }
            else
                needFindCurrentLocation=true;

        }
        if(!choose_location || (choose_location && needFindCurrentLocation)) {
            List<String> addresess = new ArrayList<>();
            LatLng currentlatLng = JobimLocation.getInstance(getContext()).getCurrentLocationLatLng();
            String currentAdress;
            if (currentlatLng != null) {
                currentAdress = JobimLocation.getInstance(getContext()).getAddressByLatLng(currentlatLng);
                addresess.add(currentAdress);
                mAdapter = new LocationAdapter(addresess, getActivity());
                mLocationRecyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }
        }

        return rootview;
    }



    @Override
    public String getParams() {
        String result="";
        if(mAdapter!=null)
            if(mAdapter.mCheckedPosition==-1){
                result="error";
                Toast.makeText(getActivity(),"חובה לבחור כתובת מהרשימה",Toast.LENGTH_SHORT).show();
            }
            else {
                result = mAdapter.getAddresses().get(mAdapter.mCheckedPosition);
                et_adress.setText(result);
            }
        return result;
    }

    @Override
    public void processFinish(List<String> output) {
        Log.e("processFinish", "done1");
        mAdapter = new LocationAdapter(output, getActivity());
        mLocationRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        Log.e("processFinish", "done2");
    }


    public void changeTitle(int choose_location) {
        title.setText(choose_location);
    }
}
