package il.co.hyperactive.jobim.Adapters_ViewHolder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

import il.co.hyperactive.jobim.Model.SearchJobSingleton;
import il.co.hyperactive.jobim.R;

/**
 * Created by Tom on 24/05/2017.
 */
public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.CompanyHolder> {

    private List<String> companies;
    private Context context;
    public int mCheckedPosition=-1;
    SearchJobSingleton searchJobSingleton;
    public CompanyAdapter(List<String> companies, Context context,int mCheckedPosition) {
        this.companies = companies;
        this.context=context;
        this.mCheckedPosition=mCheckedPosition;
        searchJobSingleton=SearchJobSingleton.getInstance();
    }

    @Override
    public CompanyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.line_content_company, parent, false);
        return new CompanyHolder(view);
    }

    @Override
    public void onBindViewHolder(final CompanyHolder holder, int position) {
        String companyName = companies.get(position);
        holder.bindCompany(companyName);
        holder.radioButton.setOnCheckedChangeListener(null);
        holder.radioButton.setChecked(position == mCheckedPosition);
        holder.radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCheckedPosition = holder.getAdapterPosition();
                notifyDataSetChanged();
            }
        });
        searchJobSingleton.setSelectedCompanyPosition(getSelectedCompanyPosition());

    }

    @Override
    public int getItemCount() {
        return companies.size();
    }

    public class CompanyHolder extends RecyclerView.ViewHolder{

        RadioButton radioButton;
        TextView textView;

        public CompanyHolder(View itemView) {
            super(itemView);
            radioButton=(RadioButton)itemView.findViewById(R.id.line_content_company_radioButton);
            textView=(TextView)itemView.findViewById(R.id.line_content_company_textView);
        }

        public void bindCompany(String companyName) {
            textView.setText(companyName);

        }
    }
    public int getSelectedCompanyPosition() {
       return mCheckedPosition;
    }
}
