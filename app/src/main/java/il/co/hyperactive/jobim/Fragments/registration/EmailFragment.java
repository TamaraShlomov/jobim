package il.co.hyperactive.jobim.Fragments.registration;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import il.co.hyperactive.jobim.Model.SharedPreferencesSingleton;
import il.co.hyperactive.jobim.Model.User;
import il.co.hyperactive.jobim.Model.Validation;
import il.co.hyperactive.jobim.R;

/**
 * Created by Tom on 08/05/2017.
 */

public class EmailFragment extends RegistrationInputFragment {
    EditText email;
    final String TAG="Email";
    public static final String FRAGMENT_NAME="EmailFragment";
    public EmailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_email, container, false);
        email=(EditText)rootView.findViewById(R.id.edittext_email);

        if(SharedPreferencesSingleton.getInstance(getActivity()).readUser()!=null){
            User user=SharedPreferencesSingleton.getInstance(getActivity()).readUser();
            email.setText(user.email);

        }
        return rootView;
    }

    @Override
    public String getParams() {
        String result=email.getText().toString();
        if(!Validation.isValidField(TAG,email.getText().toString())){
            email.setBackground(getResources().getDrawable(R.drawable.edit_text_user_input_error));
            Toast.makeText(getActivity(),"you must enter a valid email",Toast.LENGTH_SHORT).show();
            result="error";
        }
        else
            email.setBackground(getResources().getDrawable(R.drawable.edit_text_user_input));
        return result;
    }
}
