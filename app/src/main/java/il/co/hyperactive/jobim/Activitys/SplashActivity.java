package il.co.hyperactive.jobim.Activitys;

import android.Manifest;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccessToken;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.accountkit.ui.SkinManager;
import com.facebook.accountkit.ui.UIManager;

import java.util.HashMap;
import java.util.Map;

import il.co.hyperactive.jobim.Model.JobimLocation;
import il.co.hyperactive.jobim.Model.FireBaseSingleton;
import il.co.hyperactive.jobim.Model.NotificationSettings;
import il.co.hyperactive.jobim.Model.SharedPreferencesSingleton;
import il.co.hyperactive.jobim.Model.User;
import il.co.hyperactive.jobim.R;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;

public class SplashActivity extends AppCompatActivity {

    private static final int FRAMEWORK_REQUEST_CODE = 1;
    private int nextPermissionsRequestCode = 4000;
    private final Map<Integer, SplashActivity.OnCompleteListener> permissionsListeners = new HashMap<>();
    FireBaseSingleton fireBaseSingleton;

    private interface OnCompleteListener {
        void onComplete();
    }

    @Override
    protected void onResume() {
        super.onResume();
        fireBaseSingleton.restartCounter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        fireBaseSingleton=FireBaseSingleton.getInstance(SplashActivity.this);
        fireBaseSingleton.restartCounter();
        fireBaseSingleton.addValueEventListnerToJobs();
        fireBaseSingleton.addValueEventListnerToIndexes();
        fireBaseSingleton.addValueEventListnerToUserJobs();
        JobimLocation.getInstance(SplashActivity.this);
        String phoneNumber=SharedPreferencesSingleton.getInstance(getBaseContext()).readPhoneNumber();
        if(!phoneNumber.equals("0")) {
            fireBaseSingleton.addValueEventListnerToFavoriteJobs(phoneNumber);
            fireBaseSingleton.addValueEventListnerToRemovedJobs(phoneNumber);
            fireBaseSingleton.addValueEventListnerToTurnToEmployer(phoneNumber);
            fireBaseSingleton.addValueEventListnerToSmartAgent(phoneNumber);
            NotificationSettings temp=SharedPreferencesSingleton.
                    getInstance(getBaseContext()).readNotificationFilter();
            if(temp!=null) {
                final NotificationSettings notificationSettings=NotificationSettings.getInstance();
                notificationSettings.setOn(temp.isOn());
                notificationSettings.setSelectedLocation(temp.getSelectedLocation());
                notificationSettings.setSelectedRadius(temp.getSelectedRadius());
                notificationSettings.setSelectedJobs(temp.getSelectedJobs());
            }

        }
    }
    public void start() {
        if (AccountKit.getCurrentAccessToken() != null) {//TokenActivity.class
            User user=SharedPreferencesSingleton.getInstance(getBaseContext()).readUser();
            if(user!=null)
                startActivity(new Intent(SplashActivity.this, NavigationActivity.class));
            else
                startActivity(new Intent(SplashActivity.this,UserRegistrationActivity.class));

        }
        else {
            SharedPreferencesSingleton.getInstance(getBaseContext()).delete();
            onLoginPhone();
        }
    }

    public void onLoginPhone() {
        onLogin(LoginType.PHONE);
    }

    private void onLogin(final LoginType loginType) {
        final Intent intent = new Intent(this, AccountKitActivity.class);
        final AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder
                = new AccountKitConfiguration.AccountKitConfigurationBuilder(
                loginType,
                AccountKitActivity.ResponseType.TOKEN);

        ///
        configurationBuilder.setSMSWhitelist(new String[]{"IL"});
        UIManager uiManager;
        final @ColorInt int primaryColor = ContextCompat.getColor(this, R.color.orange_color);
        final @DrawableRes int backgroundImage=R.color.orange_color;

        uiManager = new SkinManager(SkinManager.Skin.CONTEMPORARY,
                primaryColor,
                backgroundImage,
                SkinManager.Tint.WHITE,
                57.0);
        configurationBuilder.setUIManager(uiManager);

        final AccountKitConfiguration configuration = configurationBuilder.build();
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configuration);
        SplashActivity.OnCompleteListener completeListener = new SplashActivity.OnCompleteListener() {
            @Override
            public void onComplete() {
                startActivityForResult(intent, FRAMEWORK_REQUEST_CODE);
            }
        };
        switch (loginType) {
            case PHONE:
                if (configuration.isReceiveSMSEnabled()) {
                    final SplashActivity.OnCompleteListener receiveSMSCompleteListener = completeListener;
                    completeListener = new SplashActivity.OnCompleteListener() {
                        @Override
                        public void onComplete() {
                            requestPermissions(
                                    Manifest.permission.RECEIVE_SMS,
                                    R.string.permissions_receive_sms_title,
                                    R.string.permissions_receive_sms_message,
                                    receiveSMSCompleteListener);
                        }
                    };
                }
                if (configuration.isReadPhoneStateEnabled()) {
                    final SplashActivity.OnCompleteListener readPhoneStateCompleteListener = completeListener;
                    completeListener = new SplashActivity.OnCompleteListener() {
                        @Override
                        public void onComplete() {
                            requestPermissions(
                                    Manifest.permission.READ_PHONE_STATE,
                                    R.string.permissions_read_phone_state_title,
                                    R.string.permissions_read_phone_state_message,
                                    readPhoneStateCompleteListener);
                        }
                    };
                }
                break;
        }
        completeListener.onComplete();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode != FRAMEWORK_REQUEST_CODE) {
            return;
        }

        final String toastMessage;
        final AccountKitLoginResult loginResult = AccountKit.loginResultWithIntent(data);
        if (loginResult == null || loginResult.wasCancelled()) {
            toastMessage = "Login Cancelled";
        } else if (loginResult.getError() != null) {
            toastMessage = loginResult.getError().getErrorType().getMessage();
            final Intent intent = new Intent(this, ErrorActivity.class);
            intent.putExtra(ErrorActivity.HELLO_TOKEN_ACTIVITY_ERROR_EXTRA, loginResult.getError());
            startActivity(intent);
        } else {
            final AccessToken accessToken = loginResult.getAccessToken();
            final long tokenRefreshIntervalInSeconds =
                    loginResult.getTokenRefreshIntervalInSeconds();
            if (accessToken != null) {
                toastMessage = "Success:" + accessToken.getAccountId()
                        + tokenRefreshIntervalInSeconds;
                startActivity(new Intent(this, /*TokenActivity.class*/UserRegistrationActivity.class));
            } else {
                toastMessage = "Unknown response type";
            }
        }
        Log.e("toastMessage",toastMessage);
        Toast.makeText(
                this,
                toastMessage,
                Toast.LENGTH_LONG)
                .show();
    }


    private void requestPermissions(
            final String permission,
            final int rationaleTitleResourceId,
            final int rationaleMessageResourceId,
            final SplashActivity.OnCompleteListener listener) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if (listener != null) {
                listener.onComplete();
            }
            return;
        }

        checkRequestPermissions(
                permission,
                rationaleTitleResourceId,
                rationaleMessageResourceId,
                listener);
    }

    @TargetApi(23)
    private void checkRequestPermissions(
            final String permission,
            final int rationaleTitleResourceId,
            final int rationaleMessageResourceId,
            final SplashActivity.OnCompleteListener listener) {
        if (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
            if (listener != null) {
                listener.onComplete();
            }
            return;
        }

        final int requestCode = nextPermissionsRequestCode++;
        permissionsListeners.put(requestCode, listener);

        if (shouldShowRequestPermissionRationale(permission)) {
            new AlertDialog.Builder(this)
                    .setTitle(rationaleTitleResourceId)
                    .setMessage(rationaleMessageResourceId)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, final int which) {
                            requestPermissions(new String[] { permission }, requestCode);
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, final int which) {
                            // ignore and clean up the listener
                            permissionsListeners.remove(requestCode);
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } else {
            requestPermissions(new String[]{ permission }, requestCode);
        }
    }

    @TargetApi(23)
    @SuppressWarnings("unused")
    @Override
    public void onRequestPermissionsResult(final int requestCode,
                                           final @NonNull String permissions[],
                                           final @NonNull int[] grantResults) {
        final SplashActivity.OnCompleteListener permissionsListener = permissionsListeners.remove(requestCode);
        if (permissionsListener != null
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            permissionsListener.onComplete();
        }
    }

}
