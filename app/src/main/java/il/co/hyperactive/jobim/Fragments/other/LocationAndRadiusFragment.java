package il.co.hyperactive.jobim.Fragments.other;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;

import il.co.hyperactive.jobim.Fragments.createNewJob.JobLocationFragment;
import il.co.hyperactive.jobim.Fragments.searchJobs.JobProfessionMultiChoiseFragment;
import il.co.hyperactive.jobim.Model.NotificationSettings;
import il.co.hyperactive.jobim.R;

public class LocationAndRadiusFragment extends Fragment {

    private NumberPicker numberPicker;
    private int selectedRadius;
    private FragmentManager fm;
    private Fragment fragment;
    NotificationSettings notificationSettings;
    public LocationAndRadiusFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_location_and_radius, container, false);
        notificationSettings=NotificationSettings.getInstance();
        numberPicker=(NumberPicker)rootView.findViewById(R.id.fragment_location_and_radius_number_picker);
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(100);
        if(notificationSettings.getSelectedRadius()!=-1) {
            numberPicker.setValue(notificationSettings.getSelectedRadius());
        }
        selectedRadius=numberPicker.getValue();
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {

            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                selectedRadius=newVal;
                notificationSettings.setSelectedRadius(selectedRadius);
                Log.i("tom","selectedRadius= "+selectedRadius);
            }
        });

        initFragment();
       return rootView;
    }

    private void initFragment() {
        fm = getActivity().getSupportFragmentManager();
        fragment = fm.findFragmentById(R.id.fragment_location_and_radius_continer);

        if(fragment==null) {
            fragment =  JobLocationFragment.newInstance(true);
            fm.beginTransaction()
                    .add(R.id.fragment_location_and_radius_continer, fragment)
                    .commit();
        }
        else {
            fragment =  JobLocationFragment.newInstance(true);
            fm.beginTransaction()
                    .replace(R.id.fragment_location_and_radius_continer, fragment)
                    .commit();
        }
    }
}
