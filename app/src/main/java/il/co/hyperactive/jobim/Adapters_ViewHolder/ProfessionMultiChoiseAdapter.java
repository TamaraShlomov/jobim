package il.co.hyperactive.jobim.Adapters_ViewHolder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.android.gms.cast.CastRemoteDisplayLocalService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import il.co.hyperactive.jobim.Model.NotificationSettings;
import il.co.hyperactive.jobim.Model.Profession;
import il.co.hyperactive.jobim.Model.SearchJobSingleton;
import il.co.hyperactive.jobim.R;

/**
 * Created by Tom on 24/05/2017.
 */
public class ProfessionMultiChoiseAdapter extends RecyclerView.Adapter<ProfessionMultiChoiseAdapter.ProfessionHolder>
        implements View.OnClickListener{

    private List<Profession> mProfessions;
    private Context context;
    public List<Boolean> mCheckBoxStates;
    int count;
    SearchJobSingleton searchJobSingleton;
    boolean searchJob;
    NotificationSettings notificationSettings;
    public ProfessionMultiChoiseAdapter(List<Profession> professions, Context context,
                                        List<Integer> selectedPositions,boolean searchJob) {
        mProfessions = professions;
        this.context=context;
        mCheckBoxStates=new ArrayList<>(mProfessions.size());
        int length=mProfessions.size();
        while (length>=0) {
            mCheckBoxStates.add(false);
            length--;
        }
        if(selectedPositions!=null) {
            for (Integer position : selectedPositions)
                mCheckBoxStates.set(position, true);
        }
        count=0;
        this.searchJob=searchJob;
        if(searchJob)
         searchJobSingleton=SearchJobSingleton.getInstance();
        else
            notificationSettings=NotificationSettings.getInstance();
    }

    @Override
    public ProfessionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.line_content_profession_check_box, parent, false);
        view.setOnClickListener(this);
        view.setTag(count);
        count++;
        return new ProfessionHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProfessionHolder holder,  int position) {
        Profession profession = mProfessions.get(position);
        holder.bindProfession(profession);
        holder.checkBox.setOnCheckedChangeListener(null);
        holder.checkBox.setClickable(false);
        holder.checkBox.setChecked(mCheckBoxStates.get(position));

    }

    @Override
    public int getItemCount() {
        return mProfessions.size();
    }

    @Override
    public void onClick(View v) {
        int position=(int)v.getTag();
        mCheckBoxStates.set(position,!mCheckBoxStates.get(position));
        CheckBox checkBox=(CheckBox) v.findViewById(R.id.line_content_profession_check_box);
        checkBox.setChecked(mCheckBoxStates.get(position));
        if(searchJob)
        searchJobSingleton.setSelectedJobs(getSelectedJobs());
        else
            notificationSettings.setSelectedJobs(getSelectedJobs());

    }

    public class ProfessionHolder extends RecyclerView.ViewHolder{

        CheckBox checkBox;
        TextView textView;
        ImageView imageView;

        public ProfessionHolder(View itemView) {
            super(itemView);
            checkBox=(CheckBox) itemView.findViewById(R.id.line_content_profession_check_box);
            textView=(TextView)itemView.findViewById(R.id.line_content_profession_textView);
            imageView=(ImageView)itemView.findViewById(R.id.line_content_profession_imageView);
        }

        public void bindProfession(Profession profession) {
            textView.setText(profession.getString_id_textView());
            imageView.setImageResource(profession.getResuorce_id_imageView());
        }
    }

    public List<Integer> getSelectedJobs(){
        List<Integer> selectedJobs=new ArrayList<>();
            for(int i=0;i<mCheckBoxStates.size();i++)
                if(mCheckBoxStates.get(i))
                    selectedJobs.add(i);
        return selectedJobs;
    }
}
