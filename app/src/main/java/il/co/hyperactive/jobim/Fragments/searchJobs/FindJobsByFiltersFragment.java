package il.co.hyperactive.jobim.Fragments.searchJobs;

/**
 * Created by Tom on 22/06/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;


import il.co.hyperactive.jobim.Fragments.findJobs.FindJobsFragment;
import il.co.hyperactive.jobim.Fragments.findJobs.JobListFragment;
import il.co.hyperactive.jobim.Interface.Filter;
import il.co.hyperactive.jobim.Model.CompanyLab;
import il.co.hyperactive.jobim.Model.FireBaseSingleton;
import il.co.hyperactive.jobim.Model.Job;
import il.co.hyperactive.jobim.Model.JobimLocation;
import il.co.hyperactive.jobim.Model.ProfessionLab;
import il.co.hyperactive.jobim.Model.SearchJobSingleton;
import il.co.hyperactive.jobim.R;

public class FindJobsByFiltersFragment extends Fragment implements View.OnClickListener {
    FragmentManager fm;
    Fragment fragment1;
    private Filter delegate;
    TextView tv_clean,tv_find_jobs_by_selected_filters;
    View rootView;
    SearchJobSingleton searchJobSingleton;
    List<Integer> selectedJobsPosition;
    LatLng selectedLocation;
    int selectedCompanyPosition;
    FireBaseSingleton fireBaseSingleton;
    public FindJobsByFiltersFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
         rootView= inflater.inflate(R.layout.fragment_find_jobs_by_filters, container, false);
         tv_clean=(TextView)rootView.findViewById(R.id.tv_clean);
         tv_find_jobs_by_selected_filters=(TextView)rootView.findViewById(R.id.find_jobs_by_selected_filters_tv);
         tv_clean.setOnClickListener(this);
         tv_find_jobs_by_selected_filters.setOnClickListener(this);

        searchJobSingleton=SearchJobSingleton.getInstance();
         List<Job> jobList=getJobListBySelectedFilters();
         fragment1 = JobListFragment.newInstance(jobList);
         fm.beginTransaction()
                .add(R.id.fragment_find_jobs_by_filters_continer, fragment1)
                .commit();

        return rootView;
    }

    private List<Job> getJobListBySelectedFilters() {
        List<Job> result;
        List<Job> selectedJobsByProffesion=new ArrayList<>();
        List<Job> selectedJobsByLocation=new ArrayList<>();
        List<Job> selectedJobsByCompany=new ArrayList<>();

        selectedJobsPosition=searchJobSingleton.getSelectedJobs();
        selectedLocation=searchJobSingleton.getSelectedLocation();
        selectedCompanyPosition=searchJobSingleton.getSelectedCompanyPosition();

        String disc="";
        if(selectedJobsPosition.size()==1)
            disc= getString(ProfessionLab.getInstance().
                    getProfessions().get(selectedJobsPosition.get(0)).getString_id_textView());
        else if(selectedJobsByProffesion.size()>1)
            disc=" גובים "+selectedJobsByProffesion.size()+" מציג ";

        if(selectedLocation!=null)
            disc+=" ב "+ JobimLocation.getInstance(getContext()).getAddressByLatLng(selectedLocation);

        if(selectedCompanyPosition!=-1)
           disc+=" ב "+ CompanyLab.getInstance().getCompanies().get(selectedCompanyPosition);

        tv_find_jobs_by_selected_filters.setText(disc);

        fireBaseSingleton=FireBaseSingleton.getInstance(getActivity());
        List<Job> alljobs=fireBaseSingleton.getAllJobsToShow();
        for (Job job:alljobs){
            if(selectedJobsPosition!=null) {
                for (Integer profrssionPosition : selectedJobsPosition)
                    if (job.getProffesion() == profrssionPosition) {
                        selectedJobsByProffesion.add(job);
                        break;
                    }
            }
            else
                selectedJobsByProffesion.add(job);

            if(selectedLocation!=null) {
                if (JobimLocation.getInstance(getContext()).getAddressByLatLng(selectedLocation).
                        equals(job.getJob_location()))
                    selectedJobsByLocation.add(job);
            }
            else
                selectedJobsByLocation.add(job);

            if(selectedCompanyPosition!=-1) {
                String companyName= CompanyLab.getInstance().getCompanies().get(selectedCompanyPosition);
                if (companyName.equals(job.getCompany_name()))
                    selectedJobsByCompany.add(job);
            }
            else
                selectedJobsByCompany.add(job);

        }


        List<Job> temp=getCommonJobList(selectedJobsByProffesion,selectedJobsByLocation);
        result=getCommonJobList(temp,selectedJobsByCompany);

        return result;
    }

    private List<Job> getCommonJobList(List<Job> list1, List<Job> list2) {
        List<Job> result=new ArrayList<>();
        if(list1.size()!=0 && list2.size()!=0) {
            for (Job job2 : list2)
                if(list1.contains(job2))
                    result.add(job2);
        }
        return result;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            delegate = (Filter) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement filter");
        }
        fm = getActivity().getSupportFragmentManager();
        fragment1 = fm.findFragmentById(R.id.fragment_find_jobs_by_filters_continer);

        if(fragment1!=null)
            fm.beginTransaction().attach(fragment1).commit();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        fm.beginTransaction().detach(fragment1).commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.find_jobs_by_selected_filters_tv:
                delegate.showSearchFilters();
                return;
            case R.id.tv_clean:
            searchJobSingleton.cleanAll();
            fragment1 = FindJobsFragment.newInstance(false);
            //todo: check this line addToBackStack(null)
            fm.beginTransaction()
                    .replace(R.id.app_bar_fragment_continer, fragment1)
                    .addToBackStack(null)
                    .commit();
            return;
        }
    }


}
