package il.co.hyperactive.jobim.Activitys.Not_need;

import android.Manifest;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import il.co.hyperactive.jobim.R;

public class RegistrationByPhoneActivity extends AppCompatActivity implements View.OnClickListener {
   private EditText phoneNumber;
   private TextView digit1,digit2,digit3,digit4,digit5,digit6,digit7,digit8,digit9,digit0;
   private ImageButton delete;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_by_phone);
        init();
        writeDigitToOnClickListner();

    }

    private void writeDigitToOnClickListner() {
        digit1.setOnClickListener(this);
        digit2.setOnClickListener(this);
        digit3.setOnClickListener(this);
        digit4.setOnClickListener(this);
        digit5.setOnClickListener(this);
        digit6.setOnClickListener(this);
        digit7.setOnClickListener(this);
        digit8.setOnClickListener(this);
        digit9.setOnClickListener(this);
        digit0.setOnClickListener(this);
        delete.setOnClickListener(this);
    }

    private void init() {
        digit1=(TextView) findViewById(R.id.tv_digit1);
        digit2=(TextView) findViewById(R.id.tv_digit2);
        digit3=(TextView) findViewById(R.id.tv_digit3);
        digit4=(TextView) findViewById(R.id.tv_digit4);
        digit5=(TextView) findViewById(R.id.tv_digit5);
        digit6=(TextView) findViewById(R.id.tv_digit6);
        digit7=(TextView) findViewById(R.id.tv_digit7);
        digit8=(TextView) findViewById(R.id.tv_digit8);
        digit9=(TextView) findViewById(R.id.tv_digit9);
        digit0=(TextView) findViewById(R.id.tv_digit0);
        delete=(ImageButton) findViewById(R.id.tv_delete);
        phoneNumber=(EditText)findViewById(R.id.tv_phoneNumber);


    }

    @Override
    public void onClick(View v) {
        TextView tv=(TextView)v;
        if(R.id.tv_delete==v.getId()){
            int length=phoneNumber.getText().toString().length();
            if(length>0)
            phoneNumber.setText(phoneNumber.getText().toString().substring(0,length-1));
        }
        else{
            phoneNumber.setText(phoneNumber.getText()+tv.getText().toString());
        }
        if(phoneNumber.getText().toString().length()==10){
            String verfCode=randonCode();
            sendSMS(phoneNumber.getText().toString(), verfCode);
            Intent i = new Intent(RegistrationByPhoneActivity.this, VerificationByPhoneActivity.class);
            i.putExtra("verfCode",verfCode);
            i.putExtra("phoneNumber",phoneNumber.getText().toString());
            startActivity(i);
            Log.i("tom","message send");
        }
    }

    private String randonCode(){
        String result="";
        Random r = new Random();
        int lower=0;
        int upper=9;
        for(int i=0;i<4;i++)
            result+=(int)((Math.random() * (upper - lower)) + lower);
        return result;
    }
    //Sends an SMS message to another device
    private void sendSMS(String phoneNumber, String message) {
        try {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.SEND_SMS},1);
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(phoneNumber, null, message, null, null);
            Toast.makeText(getApplicationContext(), "SMS Sent!",
                    Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(),
                    "SMS faild, please try again later!",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
}
