package il.co.hyperactive.jobim.Fragments.searchJobs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import il.co.hyperactive.jobim.Adapters_ViewHolder.CompanyAdapter;
import il.co.hyperactive.jobim.Adapters_ViewHolder.ProfessionMultiChoiseAdapter;
import il.co.hyperactive.jobim.Model.CompanyLab;
import il.co.hyperactive.jobim.Model.Profession;
import il.co.hyperactive.jobim.Model.ProfessionLab;
import il.co.hyperactive.jobim.Model.SearchJobSingleton;
import il.co.hyperactive.jobim.R;

public class JobCompanySearchFragment extends Fragment implements TextWatcher {
    View rootview;
    private RecyclerView mProfessionRecyclerView;
    private List<String> companies;
    private CompanyAdapter mAdapter;
    EditText search_company_edit_text;
    CompanyLab companyLab;
    int selectedCompanyPosition;
    SearchJobSingleton searchJobSingleton;
    public JobCompanySearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootview= inflater.inflate(R.layout.fragment_job_company_search, container, false);

        searchJobSingleton=SearchJobSingleton.getInstance();
        selectedCompanyPosition=searchJobSingleton.getSelectedCompanyPosition();
        companyLab = companyLab.getInstance();
        companies = companyLab.getCompanies();

        mProfessionRecyclerView = (RecyclerView) rootview.findViewById(R.id.company_recycler_view);
        mProfessionRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                mProfessionRecyclerView.getContext(), LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.line_seperator_gray));
        mProfessionRecyclerView.addItemDecoration(dividerItemDecoration);

        updateUI();
        search_company_edit_text=(EditText)rootview.findViewById(R.id.jon_company_et_search);
        search_company_edit_text.addTextChangedListener(this);
        return rootview;

    }

    private void updateUI() {
        mAdapter = new CompanyAdapter(companies,getActivity(),selectedCompanyPosition);
        mProfessionRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    public int getSelectedCompanyPosition() {
        int position=-1;
        if(mAdapter!=null)
            if(mAdapter.mCheckedPosition!=-1)
                position=mAdapter.mCheckedPosition;
        return position;
    }
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if(s.equals(""))
            companies = companyLab.getCompanies();
        else {
            List<String> temp = new ArrayList<>();
            for (String company : companies) {
                if (company.contains(s))
                    companies.add(company);
            }
        }
        updateUI();
    }

    @Override
    public void afterTextChanged(Editable s) {}

    @Override
    public void onPause() {
        searchJobSingleton.setSelectedCompanyPosition(getSelectedCompanyPosition());
        super.onPause();
    }
}
