package il.co.hyperactive.jobim.Fragments.findJobs;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.Serializable;
import java.util.List;

import il.co.hyperactive.jobim.Model.Job;
import il.co.hyperactive.jobim.Model.JobimLocation;
import il.co.hyperactive.jobim.Model.Profession;
import il.co.hyperactive.jobim.Model.ProfessionLab;
import il.co.hyperactive.jobim.R;

public class BigMapFragment extends Fragment{

    private MapView mMapView;
    private GoogleMap googleMap;
    private List<Job> allJobs;
    JobimLocation locationSingleton;
    public BigMapFragment(){}

    public static BigMapFragment newInstance(List<Job> jobs) {
        BigMapFragment fragment = new BigMapFragment();
        Bundle args = new Bundle();
        args.putSerializable("jobs",(Serializable) jobs);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            allJobs=(List<Job>)getArguments().getSerializable("jobs");
        }
        locationSingleton=JobimLocation.getInstance(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map_big, container, false);
        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                googleMap.setMyLocationEnabled(true); //Gets the status of the my-location layer.

                addAllMarkers();

                LatLng currentLocation = locationSingleton.getCurrentLocationLatLng();
                CameraPosition cameraPosition = new CameraPosition.Builder().target(currentLocation).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });

        return rootView;
    }

    private void addAllMarkers() {

        for (Job job:allJobs) {
            MarkerOptions markerOptions=new MarkerOptions();
            markerOptions.position(locationSingleton.getLocationLatLngFromAddress(job.getJob_location()));
            Profession profession=ProfessionLab.getInstance().getProfessions().get(job.getProffesion());
           // Drawable drawable=getResources().getDrawable(R.drawable.job_background);
            Drawable drawable=getResources().getDrawable(profession.getResuorce_id_imageView());
            Bitmap bitmap=resize(drawable);
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
            String companyname=" שם החברה : ";
            markerOptions.snippet(job.getCompany_name()+companyname);
            markerOptions.title(job.getJob_title());
            googleMap.addMarker(markerOptions);
        }

    }
    private Bitmap resize(Drawable image) {
        Bitmap b = ((BitmapDrawable)image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, 90, 90, false);
        return bitmapResized;
    }
    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
