package il.co.hyperactive.jobim.Fragments.myJobs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import il.co.hyperactive.jobim.Fragments.findJobs.JobListFragment;
import il.co.hyperactive.jobim.Model.FireBaseSingleton;
import il.co.hyperactive.jobim.Model.Job;
import il.co.hyperactive.jobim.Model.SharedPreferencesSingleton;
import il.co.hyperactive.jobim.R;

/**
 * Created by Tom on 25/06/2017.
 */

public class MyJobsFragment extends Fragment implements View.OnClickListener {
    View rootView;
    Button btn_favorite, btn_turned_to, btn_published;
    Fragment fragment;
    FragmentManager fm;
    int lastPosition;
    int currentPosition;
    List<Job> favoriteJobs,turnedToJobs,PublishedJobs; 
    FireBaseSingleton fireBaseSingleton;
    SharedPreferencesSingleton sharedPreferencesSingleton;
    public MyJobsFragment() {
        // Required empty public constructor
    }
    public static Fragment newInstance(int position){
        Bundle bundle=new Bundle();
        bundle.putInt("position",position);
        MyJobsFragment fragment = new MyJobsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.fragment_my_jobs, container, false);
        fireBaseSingleton=FireBaseSingleton.getInstance(getActivity());
        sharedPreferencesSingleton=SharedPreferencesSingleton.getInstance(getContext());
        init();
        return rootView;
    }

    private void init() {
        String phoneNumber=sharedPreferencesSingleton.readPhoneNumber(); //userId
        PublishedJobs=fireBaseSingleton.getAdvertiserAllJobs(fireBaseSingleton.getAdvertiserAllJobsId(phoneNumber));
        favoriteJobs=fireBaseSingleton.getAllFavoriteJobs();
        turnedToJobs=fireBaseSingleton.getAllTurnedToEmployerJobs();

        fm = getActivity().getSupportFragmentManager();
        fragment = fm.findFragmentById(R.id.continer);
        if(getArguments()!=null)
            lastPosition=getArguments().getInt("position");
        else
            lastPosition=R.id.fragment_my_jobs_btn_favorite;
        btn_favorite =(Button)rootView.findViewById(R.id.fragment_my_jobs_btn_favorite);
        btn_turned_to =(Button)rootView.findViewById(R.id.fragment_my_jobs_btn_turned_to);
        btn_published =(Button)rootView.findViewById(R.id.fragment_my_jobs_btn_published);
        
        btn_favorite.setOnClickListener(this);
        btn_turned_to.setOnClickListener(this);
        btn_published.setOnClickListener(this);

        if(fragment==null) {
            if(getArguments()==null) {
                fragment = JobListFragment.newInstance(favoriteJobs);
                btn_favorite.setBackgroundResource(R.drawable.background_orange_with_radius);
                btn_favorite.setTextColor(getResources().getColor(android.R.color.white));
            }
            else{
                fragment = JobListFragment.newInstance(PublishedJobs);
                btn_published.setBackgroundResource(R.drawable.background_orange_with_radius);
                btn_published.setTextColor(getResources().getColor(android.R.color.white));
            }
            fm.beginTransaction()
                    .add(R.id.continer, fragment)
                    .commit();
        }
        else
            btn_favorite.performClick();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fragment_my_jobs_btn_favorite:
                favoriteJobs=fireBaseSingleton.getAllFavoriteJobs();
                fragment = JobListFragment.newInstance(favoriteJobs);
                break;
            case R.id.fragment_my_jobs_btn_turned_to:
                fragment = JobListFragment.newInstance(turnedToJobs);
                break;
            case R.id.fragment_my_jobs_btn_published:
                fragment = JobListFragment.newInstance(PublishedJobs);
                break;
            default:
                return;
        }
        currentPosition=v.getId();
        changeColor();
        lastPosition=v.getId();
        fm.beginTransaction()
                .replace(R.id.continer, fragment)
                .commit();
    }


    private void changeColor() {
        Button btn=(Button)rootView.findViewById(lastPosition);
        btn.setBackgroundResource(android.R.color.transparent);
        btn.setTextColor(getResources().getColor(android.R.color.black));

        btn=(Button)rootView.findViewById(currentPosition);
        btn.setBackgroundResource(R.drawable.background_orange_with_radius);
        btn.setTextColor(getResources().getColor(android.R.color.white));
    }
    // לא טוב
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("tom", "result code :" + resultCode);
        Log.i("tom", "request code :" + requestCode);
        if (requestCode == 1) {
            if (resultCode == getActivity().RESULT_OK) {
                ((JobListFragment) fragment).deleteJobFromList(0);
            }
        }
    }
}
