package il.co.hyperactive.jobim.Model;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import il.co.hyperactive.jobim.R;

/**
 * Created by Tom on 17/05/2017.
 */

public class SharedPreferencesSingleton {// קוראים לזה גם manager
    private Context context;
    private SharedPreferences sharedPref;
    private static SharedPreferencesSingleton sharedPreferences_Singleton_;

    private SharedPreferencesSingleton(Context context){
         sharedPref = context.getSharedPreferences(
                context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        this.context=context;
    }
    public static SharedPreferencesSingleton getInstance(Context context){
        if(sharedPreferences_Singleton_ ==null)
            sharedPreferences_Singleton_ =new SharedPreferencesSingleton(context);
        return sharedPreferences_Singleton_;
    }
    public void WritePhoneNumber(String phoneNumber) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(context.getString(R.string.saved_user_phone_number), phoneNumber);
        editor.commit();
    }
    public String readPhoneNumber() {
        String phoneNumber = sharedPref.getString(context.getString(R.string.saved_user_phone_number), "0");
        return phoneNumber;
    }
    public void writeUser(User user){
        SharedPreferences.Editor editor = sharedPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putString("user", json);
        editor.commit();
    }
    public User readUser(){
        Gson gson = new Gson();
        String json = sharedPref.getString("user","");
        User user = gson.fromJson(json, User.class);
        return user;
    }
    public void delete(){
        sharedPref.edit().clear().commit();
//        SharedPreferences.Editor editor = context.getSharedPreferences("clear_cache", Context.MODE_PRIVATE).edit();
//        editor.clear();
//        editor.commit();
    }

    public void WriteNotificationFilter(NotificationSettings notificationSettings){
        SharedPreferences.Editor editor = sharedPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(notificationSettings);
        editor.putString("nFilter", json);
        editor.commit();
    }
    public NotificationSettings readNotificationFilter(){
        Gson gson = new Gson();
        String json = sharedPref.getString("nFilter","");
        NotificationSettings notificationSettings = gson.fromJson(json, NotificationSettings.class);
        Log.e("tom","notification object"+notificationSettings);
        return notificationSettings;
    }


}
