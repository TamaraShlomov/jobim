package il.co.hyperactive.jobim.Fragments.other;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import il.co.hyperactive.jobim.Fragments.findJobs.JobListFragment;
import il.co.hyperactive.jobim.Model.FireBaseSingleton;
import il.co.hyperactive.jobim.Model.Job;
import il.co.hyperactive.jobim.R;


public class SmartAgentFragment extends Fragment {

    private FragmentManager fm;
    private Fragment fragment;
    List<Job> smart_agent_jobs;



    public SmartAgentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       final View rootView =inflater.inflate(R.layout.fragment_smart_agent, container, false);
       smart_agent_jobs= FireBaseSingleton.getInstance(getActivity()).getAllSmartAgentJobs();
       initFragment();
       return rootView;
    }
    private void initFragment() {
        fm = getActivity().getSupportFragmentManager();
        fragment = fm.findFragmentById(R.id.continer);

        if(fragment==null) {
            fragment =  JobListFragment.newInstance(smart_agent_jobs);
            fm.beginTransaction()
                    .add(R.id.continer, fragment)
                    .commit();
        }
        else {
            fragment =  JobListFragment.newInstance(smart_agent_jobs);
            fm.beginTransaction()
                    .replace(R.id.continer, fragment)
                    .commit();
        }
    }

}
