package il.co.hyperactive.jobim.Model;

import android.content.Context;
import android.support.annotation.ColorRes;

import java.util.ArrayList;
import java.util.List;

import il.co.hyperactive.jobim.R;

/**
 * Created by Tom on 23/05/2017.
 */

public class ProfessionLab {
    private static ProfessionLab professionLab;
    private ArrayList professions;
    private ProfessionLab(){
      int[] professions_text_id={
              R.string.shift_supervisor,R.string.maintenance_Man,R.string.teens,
              R.string.berman,R.string.kindergarten_teacher_assistant,R.string.sales_attendant,
              R.string.cook,R.string.guard,R.string.host,
              R.string.seller_in_a_store,R.string.storekeeper,R.string.waiter,
              R.string.branch_Manager,R.string.refueling,R.string.driver,
              R.string.representative_service_and_sale,R.string.usher,R.string.real_estate_agent,
              R.string.hair_designer,R.string.working_from_home,R.string.work_Desk,
              R.string.kitchen_worker,R.string.cleaner,R.string.cashier,R.string.delivery_person
      };
      int[] professions_image_id={
                R.drawable.bartender_icon_trans,R.drawable.cashier_icon_trans,R.drawable.cleaner_icon_trans,
                R.drawable.clerk_icon_trans,R.drawable.cook_icon_trans,R.drawable.courier_icon_trans,
                R.drawable.gardner_icon_trans,R.drawable.gas_attend_trans,R.drawable.handiman_icon_trans,
                R.drawable.host_icon_trans,R.drawable.kitchen_icon_trans,R.drawable.security_trans,
                R.drawable.bartender_icon_trans,R.drawable.cashier_icon_trans,R.drawable.cleaner_icon_trans,
                R.drawable.clerk_icon_trans,R.drawable.cook_icon_trans,R.drawable.courier_icon_trans,
                R.drawable.gardner_icon_trans,R.drawable.gardner_icon_trans,R.drawable.handiman_icon_trans,
                R.drawable.host_icon_trans,R.drawable.kitchen_icon_trans,R.drawable.security_trans,
                R.drawable.waiter_icon_trnas
        };
      @ColorRes
        int[] professions_color_id={
                R.color.bartender,R.color.cashier, R.color.cleaner,
                R.color.clerk, R.color.cook, R.color.courier,
                R.color.gardner,
              R.color.gas_attend, R.color.handiman,
                R.color.host, R.color.kitchen, R.color.security,
                R.color.bartender, R.color.cashier, R.color.cleaner,
                R.color.clerk, R.color.cook, R.color.courier,
                R.color.gardner,
              R.color.gas_attend, R.color.handiman,
                R.color.host,
              R.color.kitchen,
              R.color.security,
                R.color.waiter
        };
        professions=new ArrayList<>(professions_text_id.length);
        for(int i=0;i<professions_text_id.length;i++)
            professions.add(new Profession(professions_text_id[i],professions_image_id[i],
                    professions_color_id[i]));
    }

    public static ProfessionLab getInstance(){
        if(professionLab==null)
            professionLab=new ProfessionLab();
        return professionLab;
    }
    public List<Profession> getProfessions(){
        return professions;
    }
    public String getProfession(int index, Context context){
      return context.getString(getProfessions().get(index).getString_id_textView());
    }
}
