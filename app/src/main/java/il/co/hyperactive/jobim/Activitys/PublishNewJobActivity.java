package il.co.hyperactive.jobim.Activitys;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import il.co.hyperactive.jobim.Fragments.createNewJob.JobProfessionFragment;
import il.co.hyperactive.jobim.Fragments.createNewJob.JobCompanyFragment;
import il.co.hyperactive.jobim.Fragments.registration.RegistrationInputFragment;
import il.co.hyperactive.jobim.Fragments.createNewJob.JobDescriptionFragment;
import il.co.hyperactive.jobim.Fragments.createNewJob.JobLocationFragment;
import il.co.hyperactive.jobim.Fragments.createNewJob.JobOptionalDetailsFragment;
import il.co.hyperactive.jobim.Model.FireBaseSingleton;
import il.co.hyperactive.jobim.Model.Job;
import il.co.hyperactive.jobim.R;

public class PublishNewJobActivity extends AppCompatActivity implements
        View.OnClickListener {
    private FragmentPagerAdapter mPagerAdapter;
    private ViewPager mViewPager;
    Toolbar myToolbar;
    FireBaseSingleton fireBaseSingleton;
    TabLayout tabLayout;
    RegistrationInputFragment[] mFragments;
    TextView next;
    String company_name,brunch,proffesion,job_title,job_discription,job_location,image_path,for_teens,filter_question;
    Job job;
    AlertDialog alertDialog;
    private View.OnClickListener onClickAlertDialogButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publish_new_job);
        fireBaseSingleton=FireBaseSingleton.getInstance(this);
        initToolBar();
        initTabsAndAdapter();
        job=new Job();
        // custom Progressdialog
        createCustomProgressDialog();

        onClickAlertDialogButton = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickAlertDialogButton(v);
            }
        };
    }

    private void initTabsAndAdapter() {
        // Create the adapter that will return a fragment for each section
     mFragments = new RegistrationInputFragment[] {
                new JobCompanyFragment(),
                new JobProfessionFragment(),
                new JobDescriptionFragment(),
                JobLocationFragment.newInstance(false),
                new JobOptionalDetailsFragment()
        };
        mPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {

            @Override
            public RegistrationInputFragment getItem(int position) {
                return mFragments[position];
            }
            @Override
            public int getCount() {
                return mFragments.length;
            }
        };
        final int[] icons=new int[]{R.drawable.ic_account_profil,R.drawable.ic_card_travel_white_24dp,
                R.drawable.ic_sort_white_24dp,R.drawable.ic_location_on_white_24dp,
                R.drawable.ic_attach_file_white_24dp};

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager)findViewById(R.id.container);
        mViewPager.setAdapter(mPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        //////////cancel the option to click on the tab////////
        LinearLayout tabStrip = ((LinearLayout)tabLayout.getChildAt(0));
        tabStrip.setEnabled(false);
        for(int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setClickable(false);
        }
        /////////////////
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            final TabLayout.Tab tab = tabLayout.getTabAt(i);
            RelativeLayout relativeLayout = (RelativeLayout)
                    LayoutInflater.from(this).inflate(R.layout.tab, tabLayout, false);
            ImageView imageview=(ImageView)relativeLayout.findViewById(R.id.btn_tab);
            imageview.setImageResource(icons[i]);
            if(i==0)
                relativeLayout.setBackground(getResources().getDrawable(R.drawable.tab_backround_orange));
            tab.setCustomView(relativeLayout);

        }
        //disable swiping
        mViewPager.beginFakeDrag();
    }

    private void initToolBar() {
        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        TextView tv_center=(TextView)myToolbar.findViewById(R.id.tv_toolbar_center);
        tv_center.setText(R.string.publishing_new_job);
        tv_center.setVisibility(View.VISIBLE);
        next=(TextView)myToolbar.findViewById(R.id.tv_action_left);
        next.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.right_side_toolbar, menu);
        MenuItem exitItem = menu.findItem(R.id.menu_item_exit);
        exitItem.setVisible(true);
        exitItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
               // finish();
                createAlertDialog();
                return false;
            }
        });

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_exit:
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        hideKeyBoard(v);
        switch (v.getId()) {
            case R.id.tv_action_left:
                int position = tabLayout.getSelectedTabPosition();
                String[] params;
                if (mFragments[position].getParams() == "error")
                    return;
                switch (position) {
                    case 0:
                        params = mFragments[0].getParams().split(",");
                         job.setCompany_name(params[0]);
                        if(params.length>1)
                             job.setBrunch(params[1]);
                        break;
                    case 1:
                         job.setProffesion(Integer.parseInt(mFragments[1].getParams()));
                        break;
                    case 2:
                        params = mFragments[2].getParams().split(",");
                         job.setJob_title(params[0]);
                        if(params.length>1)
                            job.setJob_discription(params[1]);
                        break;
                    case 3:
                         job.setJob_location(mFragments[3].getParams());
                        break;
                    case 4:
                        params = mFragments[4].getParams().split(",");
                         job.setImage_path(params[0]);
                         job.setFor_teens(params[1]=="yes");
                         job.setFilter_question(params[2]);
                        break;
                }

                if (position + 1 == tabLayout.getTabCount()) {
                    Intent intent=new Intent(PublishNewJobActivity.this,ContactInformationActivity.class);
                    intent.putExtra("job",job);
                    startActivity(intent);
                }
                else {
                    if (position + 1 == 4)
                        next.setText(R.string.save);

                    tabLayout.getTabAt(position + 1).select();
                    tabLayout.getTabAt(position + 1).getCustomView().
                            setBackground(getResources().getDrawable(R.drawable.tab_backround_orange));
                    tabLayout.getTabAt(position).setIcon(R.drawable.ic_done_white_24dp);

                }
        }

    }

    private void hideKeyBoard(View v) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public void createCustomProgressDialog() {

        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.show();
        dialog.setContentView(R.layout.waiting_image_views_with_animation);
        final ImageView image_world = (ImageView) dialog.findViewById(R.id.image_view_world);
        final ImageView image_job = (ImageView) dialog.findViewById(R.id.image_view_job_icon);
        final Animation animationMove = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate);
        final  Animation animationRotate = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.move);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    image_world.startAnimation(animationMove);
                    image_job.startAnimation(animationRotate);
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        }).start();
    }

    public void createAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        View mView= getLayoutInflater().inflate(R.layout.custom_alert_dialog,null);
        Button btn_right,btn_left;
        btn_left=(Button)mView.findViewById(R.id.custom_alert_dialog_btn_left);
        btn_right=(Button)mView.findViewById(R.id.custom_alert_dialog_btn_right);

        btn_left.setOnClickListener(onClickAlertDialogButton);
        btn_right.setOnClickListener(onClickAlertDialogButton);

        alertDialogBuilder.setView(mView);
        alertDialog=alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();
    }

    public void onClickAlertDialogButton(View view) {
        switch (view.getId()){
            case R.id.custom_alert_dialog_btn_right:
                alertDialog.cancel();
                break;
            case R.id.custom_alert_dialog_btn_left:
                this.finish();
                break;
        }
    }
}
