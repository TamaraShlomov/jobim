package il.co.hyperactive.jobim.Fragments.findJobs;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.Serializable;
import java.util.List;

import il.co.hyperactive.jobim.Adapters_ViewHolder.JobAdapter;
import il.co.hyperactive.jobim.Interface.delete;
import il.co.hyperactive.jobim.Model.Job;
import il.co.hyperactive.jobim.R;

public class JobListFragment extends Fragment implements delete {
    List<Job> allJobs;
    View rootView;
    RecyclerView mRecyclerView;
    ImageView imageView_no_found_job;
    JobAdapter adapter;
    public JobListFragment() {
        // Required empty public constructor
    }
    public static JobListFragment newInstance(List<Job> jobs) {
        JobListFragment fragment = new JobListFragment();
        Bundle args = new Bundle();
        args.putSerializable("jobs",(Serializable) jobs);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            allJobs=(List<Job>)getArguments().getSerializable("jobs");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.fragment_job_list, container, false);
        mRecyclerView = (RecyclerView)rootView.findViewById(R.id.recyclerview);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
//        mRecyclerView.getLayoutManager().setAutoMeasureEnabled(true);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true); //todo: I think it makes the scroll faster-ask shay

        //
        imageView_no_found_job = (ImageView) rootView.findViewById(R.id.image_view_no_found_jobs);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.line_seperator));
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        if(allJobs!=null && allJobs.size()>0) {
            imageView_no_found_job.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            //todo: i dont know if this change anything- ask shay
            new Thread(new Runnable() {
                public void run() {
                    // a potentially  time consuming task
                    adapter = new JobAdapter(allJobs, getContext(),getActivity().getSupportFragmentManager(),
                            JobListFragment.this);
                    mRecyclerView.post(new Runnable() {
                        public void run() {
                            mRecyclerView.setAdapter(adapter);
                        }
                    });
                }
            }).start();
        }
        else {
            imageView_no_found_job.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
        return rootView;
    }

    @Override
    public void deleteJobFromList(int index) {
        adapter.deleteItem(index);
    }
}
