package il.co.hyperactive.jobim.Model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;


public class AdressJSONParser {

    public List<String> parse(JSONObject jObject){

        JSONArray jPlaces = null;
        try {
            jPlaces = jObject.getJSONArray("predictions");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return getPlaces(jPlaces);
    }

    private List<String> getPlaces(JSONArray jPlaces){
        int placesCount = jPlaces.length();
        List<String> placesList = new ArrayList<>();
        String city="";
        for(int i=0; i<placesCount;i++){
            try {
                /** Call getPlace with place JSON object to parse the place */
                city = getPlace((JSONObject)jPlaces.get(i));
                placesList.add(city);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return placesList;
    }

    private String getPlace(JSONObject jPlace){
        String description="";
        String address="";
        try {
            description = jPlace.getString("description");
            address=description.split(",")[0]+","+description.split(",")[1];
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return address;
    }

    public List<String> parserJson(String responce){
        List<String> places = null;
        AdressJSONParser placeJsonParser = new AdressJSONParser();
        JSONObject jObject;
        try{
            jObject = new JSONObject(responce);
            // Getting the parsed data as a List construct
            places = placeJsonParser.parse(jObject);

        }catch(Exception e){
            Log.d("Exception",e.toString());
        }
        return places;
    }
}