package il.co.hyperactive.jobim.Activitys;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import il.co.hyperactive.jobim.Fragments.registration.CityFragment;
import il.co.hyperactive.jobim.Fragments.registration.EmailFragment;
import il.co.hyperactive.jobim.Fragments.registration.NameLastNamePictureFragment;
import il.co.hyperactive.jobim.Fragments.registration.RegistrationInputFragment;
import il.co.hyperactive.jobim.Fragments.registration.YearsOfBornFragment;
import il.co.hyperactive.jobim.Model.FireBaseSingleton;
import il.co.hyperactive.jobim.Model.SharedPreferencesSingleton;
import il.co.hyperactive.jobim.Model.User;
import il.co.hyperactive.jobim.R;

public class EditActivity extends AppCompatActivity {
    Toolbar myToolbar;
    TextView tv_left_action;
    String fragmentName;
    RegistrationInputFragment mFragment;
    User user;
    SharedPreferencesSingleton sharedPreferencesSingleton;
    FireBaseSingleton fireBaseSingleton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_activity);
        fireBaseSingleton=FireBaseSingleton.getInstance(this);
        sharedPreferencesSingleton=SharedPreferencesSingleton.getInstance(getBaseContext());
        user=sharedPreferencesSingleton.readUser();
        fragmentName=getIntent().getStringExtra("FragmentName");

        initToolBar();

        switch (fragmentName){
            case NameLastNamePictureFragment.FRAGMENT_NAME:
                mFragment=new NameLastNamePictureFragment();
                break;
            case CityFragment.FRAGMENT_NAME:
                mFragment=new CityFragment();
                break;
            case YearsOfBornFragment.FRAGMENT_NAME:
                mFragment=new YearsOfBornFragment();
                break;
            case EmailFragment.FRAGMENT_NAME:
                mFragment=new EmailFragment();
                break;
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.frameLayout, mFragment).commit();
    }

    private void initToolBar() {
        myToolbar=(Toolbar)findViewById(R.id.toolbar_edit_name);
        tv_left_action=(TextView)findViewById(R.id.tv_action_left);

        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        tv_left_action.setText(R.string.save);
        tv_left_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String result= mFragment.getParams();
                if(result!="error") {
                    switch (fragmentName) {
                        case NameLastNamePictureFragment.FRAGMENT_NAME:
                            String[] params = result.split(",");
                            user.name = params[0];
                            user.lastname = params[1];
                            user.photoPath=params[2];
                            break;
                        case CityFragment.FRAGMENT_NAME:
                            user.city=result;
                            break;
                        case YearsOfBornFragment.FRAGMENT_NAME:
                            user.year=result;
                            break;
                        case EmailFragment.FRAGMENT_NAME:
                            user.email=result;
                            break;
                    }
                    sharedPreferencesSingleton.writeUser(user);
                    String phoneNumber=sharedPreferencesSingleton.readPhoneNumber();
                    fireBaseSingleton.writeNewUser(phoneNumber,user);
                    finish();
                }
            }

        });
        TextView toolbar_center=((TextView)myToolbar.findViewById(R.id.tv_toolbar_center));
        int  title=R.string.edit_name_and_picture;
        switch (fragmentName){
            case NameLastNamePictureFragment.FRAGMENT_NAME:
                title=R.string.edit_name_and_picture;
                break;
            case CityFragment.FRAGMENT_NAME:
                title=R.string.edit_city;
                break;
            case YearsOfBornFragment.FRAGMENT_NAME:
                title=R.string.edit_year;
                break;
            case EmailFragment.FRAGMENT_NAME:
                title=R.string.edit_email;
                break;

        }
        toolbar_center.setText(title);
        toolbar_center.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem backItem=menu.findItem(R.id.menu_item_back);
        backItem.setVisible(true);
        backItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                finish();
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.right_side_toolbar, menu);
        return true;

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
                return super.onOptionsItemSelected(item);
    }
}
