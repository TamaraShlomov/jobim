package il.co.hyperactive.jobim.Fragments.registration;

import android.support.v4.app.Fragment;
import android.view.WindowManager;

/**
 * Created by Tom on 16/05/2017.
 */

abstract public class RegistrationInputFragment extends Fragment {
    abstract public String getParams();

    public void hideKeyBoardInFragment() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

}
