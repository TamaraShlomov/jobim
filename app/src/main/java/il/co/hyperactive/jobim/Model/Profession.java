package il.co.hyperactive.jobim.Model;

/**
 * Created by Tom on 23/05/2017.
 */

public class Profession {
    private int string_id_textView;
    private int resuorce_id_imageView;
    private int resuorce_id_color;

    public Profession(int string_id_textView, int resuorce_id_imageView,int resuorce_id_color) {
        this.string_id_textView = string_id_textView;
        this.resuorce_id_imageView = resuorce_id_imageView;
        this.resuorce_id_color=resuorce_id_color;
    }

    public int getResuorce_id_color() {
        return resuorce_id_color;
    }

    public void setResuorce_id_color(int resuorce_id_color) {
        this.resuorce_id_color = resuorce_id_color;
    }

    public int getString_id_textView() {
        return string_id_textView;
    }

    public void setString_id_textView(int string_id_textView) {
        this.string_id_textView = string_id_textView;
    }

    public int getResuorce_id_imageView() {
        return resuorce_id_imageView;
    }

    public void setResuorce_id_imageView(int resuorce_id_imageView) {
        this.resuorce_id_imageView = resuorce_id_imageView;
    }
}
