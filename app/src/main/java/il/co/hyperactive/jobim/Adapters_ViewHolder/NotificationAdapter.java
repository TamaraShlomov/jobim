package il.co.hyperactive.jobim.Adapters_ViewHolder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

import il.co.hyperactive.jobim.Activitys.NavigationActivity;
import il.co.hyperactive.jobim.Model.Profession;
import il.co.hyperactive.jobim.R;

/**
 * Created by Tom on 24/05/2017.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationHolder> {

    private List<String> date_list;
    private Context context;
    public NotificationAdapter(List<String> dates, Context context) {
        date_list = dates;
        this.context=context;
    }

    @Override
    public NotificationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.line_content_notification, parent, false);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NavigationActivity)context).goToSmartAgent();
            }
        });
        return new NotificationHolder(view);
    }

    @Override
    public void onBindViewHolder(final NotificationHolder holder, int position) {
        String date = date_list.get(position);
        holder.bindProfession(date);
    }

    @Override
    public int getItemCount() {
        return date_list.size();
    }



    public class NotificationHolder extends RecyclerView.ViewHolder{
        TextView textView;

        public NotificationHolder(View itemView) {
            super(itemView);
            textView=(TextView)itemView.findViewById(R.id.line_content_noti_text_view_date);
        }

        public void bindProfession(String date) {
            textView.setText(date);
        }
    }
}
