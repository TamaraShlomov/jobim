package il.co.hyperactive.jobim.Activitys;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import il.co.hyperactive.jobim.Fragments.findJobs.JobItemIncapsulatedFragment;
import il.co.hyperactive.jobim.Fragments.findJobs.JobListFragment;
import il.co.hyperactive.jobim.Interface.Filter;
import il.co.hyperactive.jobim.Interface.delete;
import il.co.hyperactive.jobim.Model.Job;
import il.co.hyperactive.jobim.R;

public class MoreJobsFromThisEmployerActivity extends AppCompatActivity implements delete{
    List<Job> alljobs;
    android.support.v7.widget.Toolbar myToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_jobs_from_this_employer);
        initToolbar();
        alljobs=(ArrayList<Job>)getIntent().getSerializableExtra("jobs");
        // Inflate the layout for this fragment
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment1 = fm.findFragmentById(R.id.job_list_fragment);

        if (fragment1 == null) {
            fragment1 = JobListFragment.newInstance(alljobs);
            fm.beginTransaction()
                    .add(R.id.job_list_fragment, fragment1)
                    .commit();
        }

    }

    private void initToolbar() {
        myToolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        myToolbar.findViewById(R.id.tv_action_left).setVisibility(View.GONE);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        TextView center=(TextView)myToolbar.findViewById(R.id.tv_toolbar_center);
        center.setText(R.string.more_jobs_from_this_advertiser);
        center.setVisibility(View.VISIBLE);
    }
    @Override
    public void deleteJobFromList(int position) {
        JobListFragment jobListFragment = (JobListFragment)
                getSupportFragmentManager().findFragmentById(R.id.job_list_fragment);
        if (jobListFragment != null) {
            jobListFragment.deleteJobFromList(position);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == JobItemIncapsulatedFragment.PICK_JOB_TO_DELETE_REQUEST) {
            if (resultCode == Activity.RESULT_OK ) {
                deleteJobFromList(data.getIntExtra("position",-1));
            }
        }
    }

}
