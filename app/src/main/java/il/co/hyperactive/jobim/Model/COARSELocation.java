package il.co.hyperactive.jobim.Model;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.gavaghan.geodesy.*;

/**
 * Created by Tom on 14/06/2017.
 */

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.kishan.askpermission.AskPermission;
import com.kishan.askpermission.ErrorCallback;
import com.kishan.askpermission.PermissionCallback;
import com.kishan.askpermission.PermissionInterface;

public class COARSELocation implements PermissionCallback, ErrorCallback,
        OnSuccessListener<android.location.Location> {
    private FusedLocationProviderClient mFusedLocationClient;
    private Context context;

    private LatLng currentLocationLatLng;
    private static COARSELocation location;

    private COARSELocation(Context contex){
        this.context=contex;
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
        new AskPermission.Builder((Activity) context)
                .setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION)
                .setCallback(this)
                .setErrorCallback(this)
                .request(1);

        if (ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            Log.i("current location","no premission");
            return;
        }
        mFusedLocationClient.getLastLocation().addOnSuccessListener(this);

    }

    public static COARSELocation getInstance(Context context) {
        if(location==null)
            location=new COARSELocation(context);
        return location;
    }

    public LatLng getLocationLatLngFromAddress(String strAddress) {
        ///todo:check if this not make problem
        Locale lHebrew = new Locale("he");
        Geocoder coder = new Geocoder(context, lHebrew);
        //////////////////////////////////////////
        //  Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 1);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude() );
            Log.i("getLocationLatLngFromAd",p1.toString());


        } catch (IOException ex) {

            ex.printStackTrace();
        }
        return p1;
    }

    public LatLng getCurrentLocationLatLng(){
        Log.i("getCurrentLocationLatLn",currentLocationLatLng+"");
        LatLng defaultLocation=new LatLng(32.334230, 34.853132);//mekdonal 11 netanya
        if(currentLocationLatLng==null)
            return defaultLocation;
        return currentLocationLatLng;
    }

    public String getAddressByLatLng(LatLng latLng) {
        Geocoder geocoder;
        List<Address> addresses=new ArrayList<>();
        Locale lHebrew = new Locale("he");
        geocoder = new Geocoder(context, lHebrew);
        //geocoder = new Geocoder(context, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }

        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
//        String state = addresses.get(0).getAdminArea();
//        String country = addresses.get(0).getCountryName();
//        String postalCode = addresses.get(0).getPostalCode();
//        String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
        Log.i("getAddressByLatLng",address+","+city);
        return address+","+city;
    }

    @Override
    public void onPermissionsGranted(int i) {}
    @Override
    public void onPermissionsDenied(int i) {}
    @Override
    public void onShowRationalDialog(PermissionInterface permissionInterface, int i) {}
    @Override
    public void onShowSettings(PermissionInterface permissionInterface, int i) {}

    @Override
    public void onSuccess(android.location.Location location) {
        // Got last known location. In some rare situations this can be null.
        if (location != null) {
            double longitude=location.getLongitude();
            double latitude=location.getLatitude();
            currentLocationLatLng=new LatLng(latitude,longitude);
            Log.i("current location",location.toString());
            Toast.makeText(context,location.toString(),Toast.LENGTH_LONG).show();
        }
        else
            Log.i("current location","Null");
    }

    public  int getDestination(LatLng jobLocation,LatLng currentLocation){ //in meters

        GeodeticCalculator geoCalc = new GeodeticCalculator();

        Ellipsoid reference = Ellipsoid.WGS84;
        //TODO: CHECK WHAT THE PROBLEM HERE
        GlobalPosition pointA = new GlobalPosition(jobLocation.latitude, jobLocation.longitude, 0.0); // Point A

        GlobalPosition userPos = new GlobalPosition(currentLocation.latitude, currentLocation.longitude, 0.0); // Point B

        double distance = geoCalc.calculateGeodeticCurve(reference, userPos, pointA).getEllipsoidalDistance(); // Distance between Point A and Point B

        return (int)distance;
    }


}
