package il.co.hyperactive.jobim.Fragments.findJobs;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import il.co.hyperactive.jobim.Interface.delete;
import il.co.hyperactive.jobim.Model.FireBaseSingleton;
import il.co.hyperactive.jobim.Model.Job;
import il.co.hyperactive.jobim.Model.SharedPreferencesSingleton;
import il.co.hyperactive.jobim.R;

/**
 * Created by Tom on 05/06/2017.
 */

public class JobItemDeleteFragment extends Fragment implements View.OnClickListener {
    View rootView;
    ImageButton imageButton;
    Job job;
    FireBaseSingleton fireBaseSingleton;
    String phoneNumber;
    int position=0;
    public delete delegate;
    private AlertDialog alertDialog;
    private View.OnClickListener onClickAlertDialogButton;

    public static JobItemDeleteFragment newInstance(Job job,int position) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("job",job);
        bundle.putInt("position",position);
        JobItemDeleteFragment fragment = new JobItemDeleteFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof delete)
          delegate=(delete)context;
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            job =(Job)bundle.getSerializable("job");
            position=bundle.getInt("position");
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView=inflater.inflate(R.layout.fragment_job_item_delete,container,false);
        readBundle(getArguments());
        fireBaseSingleton=FireBaseSingleton.getInstance(getActivity());
        SharedPreferencesSingleton sharedPreferencesSingleton=SharedPreferencesSingleton.
                getInstance(getContext());
        phoneNumber=sharedPreferencesSingleton.readPhoneNumber();
        init();
        onClickAlertDialogButton = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickAlertDialogButton(v);
            }
        };
        return rootView;
    }

    private void init() {
        imageButton=(ImageButton)rootView.findViewById(R.id.job_item_image_button_delete);
        imageButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Log.e("tom","onclick");
        createAlertDialog();
    }

    public void createAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View mView= getActivity().getLayoutInflater().inflate(R.layout.custom_alert_dialog,null);
        TextView title,message;
        Button btn_right_cancel,btn_left_delete;
        title=(TextView)mView.findViewById(R.id.custom_alert_dialog_title);
        message=(TextView)mView.findViewById(R.id.custom_alert_dialog_message);
        btn_left_delete=(Button)mView.findViewById(R.id.custom_alert_dialog_btn_left);
        btn_right_cancel=(Button)mView.findViewById(R.id.custom_alert_dialog_btn_right);

        title.setText(R.string.delete_job);
        message.setText(R.string.delete_message);
        btn_left_delete.setText(R.string.delete_from_fid);
        btn_right_cancel.setText(R.string.cancel);

        btn_left_delete.setOnClickListener(onClickAlertDialogButton);
        btn_right_cancel.setOnClickListener(onClickAlertDialogButton);
        alertDialogBuilder.setView(mView);
        alertDialog=alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();
    }

    public void onClickAlertDialogButton(View view) {
        switch (view.getId()){
            case R.id.custom_alert_dialog_btn_right:
                Log.i("tom","cancel");
                alertDialog.cancel();
                break;
            case R.id.custom_alert_dialog_btn_left:
               //  continue with delete
                Log.i("tom","delete from fid");

                FireBaseSingleton.getInstance(getActivity()).WriteNewJobToRemove(job.getUid(),phoneNumber);
                delegate.deleteJobFromList(position);
                alertDialog.cancel();
                break;
            default:
                Log.i("tom","default");
                break;
        }
    }
}
