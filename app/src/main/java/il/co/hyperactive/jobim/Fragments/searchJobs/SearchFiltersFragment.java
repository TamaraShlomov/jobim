package il.co.hyperactive.jobim.Fragments.searchJobs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import il.co.hyperactive.jobim.Activitys.NavigationActivity;
import il.co.hyperactive.jobim.Fragments.findJobs.FindJobsFragment;
import il.co.hyperactive.jobim.Model.SearchJobSingleton;
import il.co.hyperactive.jobim.R;

public class SearchFiltersFragment extends Fragment implements View.OnClickListener {
    View rootView;
    private Animation animation;
    Button btn_job,btn_location,btn_company;
    Fragment fragment;
    FragmentManager fm;
    int lastPosition;
    int currentPosition;
    TextView ok_tv,cancel_tv;
    public SearchFiltersFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.fragment_search_filters, container, false);
        animation = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
                R.anim.slide);
        rootView.startAnimation(animation);
        init();
        return rootView;
    }

    private void init() {
        fm = getActivity().getSupportFragmentManager();
        fragment = fm.findFragmentById(R.id.fragment_search_filters_continer);

        lastPosition=R.id.fragment_search_filters_btn_job;
        btn_job=(Button)rootView.findViewById(R.id.fragment_search_filters_btn_job);
        btn_location=(Button)rootView.findViewById(R.id.fragment_search_filters_btn_location);
        btn_company=(Button)rootView.findViewById(R.id.fragment_search_filters_btn_company);
        ok_tv=(TextView) rootView.findViewById(R.id.fragment_search_filters_tv_ok);
        cancel_tv=(TextView) rootView.findViewById(R.id.fragment_search_filters_tv_cancel);


        btn_job.setOnClickListener(this);
        btn_location.setOnClickListener(this);
        btn_company.setOnClickListener(this);
        ok_tv.setOnClickListener(this);
        cancel_tv.setOnClickListener(this);

        if(fragment==null) {
            btn_job.setBackgroundResource(R.drawable.background_orange_with_radius);
            btn_job.setTextColor(getResources().getColor(android.R.color.white));
            fragment =  JobProfessionMultiChoiseFragment.newInstance(true);
            fm.beginTransaction()
                    .add(R.id.fragment_search_filters_continer, fragment)
                    .commit();
        }
        else
          btn_job.performClick();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fragment_search_filters_btn_job:
                fragment = JobProfessionMultiChoiseFragment.newInstance(true);
                break;
            case R.id.fragment_search_filters_btn_location:
                fragment = new SearchLocationInMapFragment();
                break;
            case R.id.fragment_search_filters_btn_company:
                fragment = new JobCompanySearchFragment();
                break;
            case R.id.fragment_search_filters_tv_ok:
                onPressOk();
                return;
            case R.id.fragment_search_filters_tv_cancel:
                Log.i("ok_cancel_action","cancel");
                SearchJobSingleton.getInstance().cleanAll();
                //todo: ask shay why its didnt work well
                //getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
                // getActivity().getSupportFragmentManager().popBackStack();

                 final FindJobsFragment fragment = (FindJobsFragment)getActivity().
                         getSupportFragmentManager().findFragmentByTag(NavigationActivity.TAG_FRAGMENT);

//                fragment = FindJobsFragment.newInstance(false);
                fm.beginTransaction()
                        .replace(R.id.app_bar_fragment_continer, fragment)
                        .commit();

                return;
            default:
                return;
        }
        currentPosition=v.getId();
        changeColor();
        lastPosition=v.getId();
        fm.beginTransaction()
                .replace(R.id.fragment_search_filters_continer, fragment)
                .commit();
    }

    private void onPressOk() {
        SearchJobSingleton searchJobSingleton=SearchJobSingleton.getInstance();
        List<Integer> selectedjobPositions=searchJobSingleton.getSelectedJobs();
        int companyPosition=searchJobSingleton.getSelectedCompanyPosition();
        LatLng selectedLocation=searchJobSingleton.getSelectedLocation();

        if(selectedjobPositions==null && companyPosition==-1 &&selectedLocation==null)
            fragment = FindJobsFragment.newInstance(false);
       else
            fragment = new FindJobsByFiltersFragment();

        fm.beginTransaction()
                .replace(R.id.app_bar_fragment_continer, fragment)
                .commit();
    }

    private void changeColor() {
       Button btn=(Button)rootView.findViewById(lastPosition);
        btn.setBackgroundResource(android.R.color.transparent);
        btn.setTextColor(getResources().getColor(android.R.color.black));

       btn=(Button)rootView.findViewById(currentPosition);
       btn.setBackgroundResource(R.drawable.background_orange_with_radius);
       btn.setTextColor(getResources().getColor(android.R.color.white));
    }


}
