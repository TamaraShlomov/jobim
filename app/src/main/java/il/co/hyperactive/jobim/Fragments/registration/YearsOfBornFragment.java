package il.co.hyperactive.jobim.Fragments.registration;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;

import il.co.hyperactive.jobim.Model.SharedPreferencesSingleton;
import il.co.hyperactive.jobim.Model.User;
import il.co.hyperactive.jobim.R;

/**
 * Created by Tom on 08/05/2017.
 */

public class YearsOfBornFragment extends RegistrationInputFragment {
    NumberPicker numberPicker;
    String selectedYear;
    public static final String FRAGMENT_NAME="YearsOfBornFragment";
    public YearsOfBornFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_year, container, false);
        numberPicker=(NumberPicker)rootView.findViewById(R.id.numberPicker);
        numberPicker.setMinValue(1900);
        numberPicker.setMaxValue(2000);
        selectedYear=numberPicker.getValue()+"";
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {

            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                selectedYear=newVal+"";
                Log.e("tom","selectedYear= "+selectedYear);
            }
        });
        if(SharedPreferencesSingleton.getInstance(getActivity()).readUser()!=null){
            User user=SharedPreferencesSingleton.getInstance(getActivity()).readUser();
            Log.e("user",user.toString());
            numberPicker.setValue(Integer.parseInt(user.year));
            selectedYear=user.year;
        }

        return rootView;
    }

    @Override
    public String getParams() {
        return selectedYear;
    }
}
