package il.co.hyperactive.jobim.Model;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by Tom on 03/07/2017.
 */

public class NotificationSettings {
    private static NotificationSettings notificationSettings;
    List<Integer> selectedJobs;
    LatLng selectedLocation;
    int selectedRadius;
    boolean on;
    private NotificationSettings(){
      //  selectedJobs=null;
      //  selectedLocation=null;
        selectedRadius=-1;
     //   on=false;
    }
    public static NotificationSettings getInstance(){
        if(notificationSettings==null)
            notificationSettings=new NotificationSettings();
        return notificationSettings;
    }
    public void cleanAll(){
        selectedJobs=null;
        selectedLocation=null;
        selectedRadius=-1;
    }

    public boolean isOn() {
        return on;
    }

    public void setOn(boolean on) {
        this.on = on;
    }

    public static void setNotificationSettings(NotificationSettings notificationSettings) {
        NotificationSettings.notificationSettings = notificationSettings;
    }

    public List<Integer> getSelectedJobs() {
        return selectedJobs;
    }

    public void setSelectedJobs(List<Integer> selectedJobs) {
        this.selectedJobs = selectedJobs;
    }

    public LatLng getSelectedLocation() {
        return selectedLocation;
    }

    public void setSelectedLocation(LatLng selectedLocation) {
        this.selectedLocation = selectedLocation;
    }

    public int getSelectedRadius() {
        return selectedRadius;
    }

    public void setSelectedRadius(int selectedRadius) {
        this.selectedRadius = selectedRadius;
    }
}
