package il.co.hyperactive.jobim.Fragments.findJobs;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kishan.askpermission.AskPermission;
import com.kishan.askpermission.ErrorCallback;
import com.kishan.askpermission.PermissionCallback;
import com.kishan.askpermission.PermissionInterface;

import de.cketti.mailto.EmailIntentBuilder;
import il.co.hyperactive.jobim.Model.FireBaseSingleton;
import il.co.hyperactive.jobim.Model.Job;
import il.co.hyperactive.jobim.Model.SharedPreferencesSingleton;
import il.co.hyperactive.jobim.R;

/**
 * Created by Tom on 05/06/2017.
 */

public class JobItemContactFragment extends Fragment implements View.OnClickListener, PermissionCallback, ErrorCallback {
    View rootView;
    ImageButton imageButton_sms,imageButton_call,imageButton_mail,imageButton_star;
    LinearLayout layout_star;
    TextView job_description;
    Job job;
    FireBaseSingleton fireBaseSingleton;
    String phoneNumber;
    boolean laayout_star_visible;
    TextView tv_add_remove_from_favorite;
    public static JobItemContactFragment newInstance(Job job,boolean layoutstar_visible) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("job",job);
        bundle.putBoolean("visible",layoutstar_visible);

        JobItemContactFragment fragment = new JobItemContactFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            job =(Job)bundle.getSerializable("job");
            laayout_star_visible=bundle.getBoolean("visible");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView=inflater.inflate(R.layout.fragment_job_item_contact,container,false);
        readBundle(getArguments());
        fireBaseSingleton=FireBaseSingleton.getInstance(getActivity());
        SharedPreferencesSingleton sharedPreferencesSingleton=SharedPreferencesSingleton.
                getInstance(getContext());
        phoneNumber=sharedPreferencesSingleton.readPhoneNumber();

        init();
        return rootView;
    }

    private void init() {
        layout_star=(LinearLayout)rootView.findViewById(R.id.layout_star);
        if(!laayout_star_visible) {
            layout_star.setVisibility(View.GONE);
            rootView.setBackgroundColor(getResources().getColor(android.R.color.white));
            job_description=(TextView)rootView.findViewById(R.id.fragment_job_item_contact_job_description_tv);
            job_description.setVisibility(View.VISIBLE);
            if(job!=null)
                job_description.setText(job.getJob_discription());
            rootView.getLayoutParams().height = FrameLayout.LayoutParams.WRAP_CONTENT;
        }
        imageButton_sms=(ImageButton)rootView.findViewById(R.id.job_item_image_button_sms);
        imageButton_call=(ImageButton)rootView.findViewById(R.id.job_item_image_button_call);
        imageButton_mail=(ImageButton)rootView.findViewById(R.id.job_item_image_button_email);
        imageButton_star=(ImageButton)rootView.findViewById(R.id.job_item_image_button_star);
        tv_add_remove_from_favorite=(TextView)rootView.findViewById(R.id.tv_add_remove_from_favorite);
        if(job!=null && job.isStar()) {
            imageButton_star.setBackgroundResource(android.R.drawable.star_on);
            tv_add_remove_from_favorite.setText(R.string.remove_job_from_favorite_without_enter);
        }

        imageButton_sms.setOnClickListener(this);
        imageButton_call.setOnClickListener(this);
        imageButton_mail.setOnClickListener(this);
        imageButton_star.setOnClickListener(this);

        if(job!=null){
            if(job.getSms().equals("")) {
                imageButton_sms.setBackgroundResource(R.drawable.sms_disabled);
                imageButton_sms.setClickable(false);
            }
            if(job.getCall().equals("")) {
                imageButton_call.setBackgroundResource(R.drawable.call_disabled);
                imageButton_call.setClickable(false);

            }
            if(job.getMail().equals("")) {
                imageButton_mail.setBackgroundResource(R.drawable.email_diabled);
                imageButton_mail.setClickable(false);
            }

        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.job_item_image_button_sms:
                    sendSMS(job.getSms(), "אשמח ליצור איתך קשר בקשר למשרה, מתי יהיה נוח? תודה");
                    fireBaseSingleton.AddToTurnedToEmployer(job, phoneNumber);
                break;
            case R.id.job_item_image_button_call:
                    makeCall();
                    fireBaseSingleton.AddToTurnedToEmployer(job,phoneNumber);
                break;
            case R.id.job_item_image_button_email:
                     sendMail();
                     fireBaseSingleton.AddToTurnedToEmployer(job,phoneNumber);
                break;
            case R.id.job_item_image_button_star:
               if(job.isStar()){
                   imageButton_star.setBackgroundResource(android.R.drawable.star_off);
                   tv_add_remove_from_favorite.setText(R.string.add_to_favourites_without_enter);
                   fireBaseSingleton.DeleteFromMyFavoriteJobs(job.getUid(),phoneNumber);
               }
               else{
                   imageButton_star.setBackgroundResource(android.R.drawable.star_on);
                   tv_add_remove_from_favorite.setText(R.string.remove_job_from_favorite_without_enter);
                   fireBaseSingleton.AddToMyFavoriteJobs(job.getUid(),phoneNumber);
               }
               job.setStar(!job.isStar());
               fireBaseSingleton.UpdateChange(job,phoneNumber);
               break;
        }
    }

    private boolean sendMail() {
        return EmailIntentBuilder.from(getContext())
                .to(job.getMail())
                .cc("tom.studentit@gmail.com")
                .subject("בקשר למשרה")
                .body("אשמח ליצור איתך קשר בקשר למשרה, מתי יהיה נוח? תודה")
                .start();
    }

    private void makeCall() {
        new AskPermission.Builder(this)
                .setPermissions(Manifest.permission.CALL_PHONE)
                .setCallback(this)
                .setErrorCallback(this)
                .request(1);
    }

    //Sends an SMS message to another device
    private void sendSMS(String phoneNumber, String message) {
            try {
                ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.SEND_SMS},1);
                SmsManager sms = SmsManager.getDefault();
                sms.sendTextMessage(phoneNumber, null, message, null, null);
                Toast.makeText(getActivity(), "SMS Sent!", Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(getActivity(),"SMS faild, please try again later!", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
    }

    @Override
    public void onPermissionsGranted(int i) {
        Toast.makeText(getContext(),"premission granted",Toast.LENGTH_SHORT).show();
        try {

            Intent my_callIntent = new Intent(Intent.ACTION_CALL);
            my_callIntent.setData(Uri.parse("tel:"+job.getCall()));
            //here the word 'tel' is important for making a call...
            startActivity(my_callIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getContext(), "Error in your phone call"+e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onPermissionsDenied(int i) {
        Toast.makeText(getContext(),"premission denied",Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onShowRationalDialog(PermissionInterface permissionInterface, int i) {

    }

    @Override
    public void onShowSettings(PermissionInterface permissionInterface, int i) {

    }
}
