package il.co.hyperactive.jobim.Fragments.findJobs;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.List;


import il.co.hyperactive.jobim.Activitys.JobItemExpendedActivity;
import il.co.hyperactive.jobim.Interface.Filter;
import il.co.hyperactive.jobim.Model.FireBaseSingleton;
import il.co.hyperactive.jobim.Model.Job;
import il.co.hyperactive.jobim.R;

public class FindJobsFragment extends Fragment implements View.OnClickListener {
    FireBaseSingleton fireBaseSingleton;
    List<Job> allJobsToShow;
    FragmentManager fm;
    Fragment fragment1;
    boolean showBigMap;
    private Filter delegate ;
    int position;
    public FindJobsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            delegate = (Filter) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement filter");
        }
    }

    public static Fragment newInstance(boolean map){
        Bundle bundle = new Bundle();
        bundle.putBoolean("map",map);

        FindJobsFragment fragment = new FindJobsFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null)
            showBigMap =bundle.getBoolean("map");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fm = getActivity().getSupportFragmentManager();
        fragment1 = fm.findFragmentById(R.id.continer);
        View rootView= inflater.inflate(R.layout.fragment_find_jobs, container, false);
        readBundle(getArguments());
        delegate.returnToFindJobsFragment();
        fireBaseSingleton=FireBaseSingleton.getInstance(getActivity());
        allJobsToShow =fireBaseSingleton.getAllJobsToShow();
        Log.i("tom","allJobsToShow ="+allJobsToShow.size()+"");
        if(fragment1!=null) {
            if (showBigMap)
                fragment1 = BigMapFragment.newInstance(allJobsToShow);
            else {
                fragment1 = JobListFragment.newInstance(allJobsToShow);
                FrameLayout view = (FrameLayout) rootView.findViewById(R.id.continer);
                setMargins(view, 20, 0, 20, 0);
            }
            fm.beginTransaction().replace(R.id.continer, fragment1).commit();
        }
        else{
            if (showBigMap)
                fragment1 = BigMapFragment.newInstance(allJobsToShow);
            else {
                fragment1 = JobListFragment.newInstance(allJobsToShow);
                FrameLayout view = (FrameLayout) rootView.findViewById(R.id.continer);
                setMargins(view, 20, 0, 20, 0);
            }

            fm.beginTransaction().add(R.id.continer, fragment1).commit();

        }

        TextView tv_search=(TextView)rootView.findViewById(R.id.find_jobs_tv);
            tv_search.setOnClickListener(this);
        return rootView;
    }


    private void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //todo: ask shay when i need use fm.beginTransaction.attach/detach(fragment)
        //fm.beginTransaction().detach(fragment1).commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.find_jobs_tv:
                delegate.showSearchFilters();
                break;
        }
    }

}
