package il.co.hyperactive.jobim.Interface;

import java.util.List;

/**
 * Created by Tom on 25/05/2017.
 */
public interface AsyncResponse {
    void processFinish(List<String> output);
}