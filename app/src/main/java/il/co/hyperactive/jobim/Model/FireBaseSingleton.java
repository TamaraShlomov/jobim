package il.co.hyperactive.jobim.Model;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.facebook.accountkit.AccountKit;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import il.co.hyperactive.jobim.Activitys.JobItemExpendedActivity;
import il.co.hyperactive.jobim.Activitys.NavigationActivity;
import il.co.hyperactive.jobim.Activitys.SplashActivity;
import il.co.hyperactive.jobim.R;

/**
 * Created by Tom on 17/05/2017.
 */

public class FireBaseSingleton  {
    private static FireBaseSingleton mFireBase;
    private DatabaseReference mDatabase;
    private List<Job> allJobs = new ArrayList<>();
    private List<String> allRemovedJobs = new ArrayList<>();
    private List<String> allFavoriteJobs = new ArrayList<>();
    private HashMap<String,String> indexes=new HashMap<>();
    private HashMap<String,List<String>> usersJobs=new HashMap<>();
    private HashMap<String,String> allUserTurnedToEmployer = new HashMap<>();
    private HashMap<String,String> allSmartAgentJobs = new HashMap<>(); //date,jobId
    int counter=0;
    Context context;
    private FireBaseSingleton() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    public static FireBaseSingleton getInstance(Context context) {
        if (mFireBase == null)
            mFireBase = new FireBaseSingleton();
        mFireBase.context=context;
        return mFireBase;
    }

    private void checkIfNeedToStartActivity() {
        if(context instanceof SplashActivity){
            String phoneNumber=SharedPreferencesSingleton.getInstance(context).readPhoneNumber();
            if(!phoneNumber.equals("0")) {
                if (counter == 7)
                    ((SplashActivity) context).start();
            }
            else
            if(counter==3)
                ((SplashActivity) context).start();
        }

    }

    public void restartCounter(){
        counter=0;
    }

    public void addValueEventListnerToJobs() {
        mDatabase.child("jobs").addValueEventListener(new ValueEventListener() {
            @Override
            public /*synchronized*/ void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null)
                    allJobs = new ArrayList<>();
                else
                    for (DataSnapshot jobSnapshot : dataSnapshot.getChildren()) {
                        Log.i("job snapshot", jobSnapshot.toString());
                        allJobs.add(jobSnapshot.getValue(Job.class));
                    }
                counter++;
                checkIfNeedToStartActivity();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("on cancelled", "loadPost:onCancelled", databaseError.toException());
            }
        });
    }

    public void addValueEventListnerToRemovedJobs(String phoneNumber) {
        mDatabase.child("removed-jobs").child(phoneNumber).
                addValueEventListener(new ValueEventListener() {
                    @Override
                    public /*synchronized*/ void onDataChange(DataSnapshot dataSnapshot) {
                        Log.i("removed-jobs snapshot", dataSnapshot.toString());
                        if (dataSnapshot.getValue() == null)
                            allRemovedJobs = new ArrayList<>();
                        else {
                            allRemovedJobs = (ArrayList<String>) dataSnapshot.getValue();
                            Log.i("allRemovedJobs",allRemovedJobs.toString());
                        }
                        counter++;
                        checkIfNeedToStartActivity();
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // Getting Post failed, log a message
                        Log.w("on cancelled", "loadPost:onCancelled", databaseError.toException());
                    }
                });
    }

    public void addValueEventListnerToFavoriteJobs(String phoneNumber) {
        mDatabase.child("Favorite-jobs").child(phoneNumber).
                addValueEventListener(new ValueEventListener() {
                    @Override
                    public /*synchronized*/ void onDataChange(DataSnapshot dataSnapshot) {
                        Log.e("favorite-jobs snapshot", dataSnapshot.toString());
                        if (dataSnapshot.getValue() == null)
                            allFavoriteJobs = new ArrayList<>();
                        else {
                            allFavoriteJobs = (ArrayList<String>) dataSnapshot.getValue();
                            Log.i("allFavoriteJobs",allFavoriteJobs.toString());

                        }
                        counter++;
                        checkIfNeedToStartActivity();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // Getting Post failed, log a message
                        Log.w("on cancelled", "loadPost:onCancelled", databaseError.toException());
                    }
                });
    }

    public void addValueEventListnerToIndexes(){
        mDatabase.child("indexes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null)
                    indexes = new HashMap<>();
                else
                    for (DataSnapshot jobSnapshot : dataSnapshot.getChildren()) {
                        Log.e(" snapshot", jobSnapshot.toString());
                        indexes.put(jobSnapshot.getKey(),jobSnapshot.getValue(String.class));
                    }
                    counter++;
                checkIfNeedToStartActivity();

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }

    public void addValueEventListnerToUserJobs(){
        mDatabase.child("user-jobs").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    usersJobs = new HashMap<>();
                    Log.i("USERJOBS", "empty");

                }
                else
                    for (DataSnapshot jobSnapshot : dataSnapshot.getChildren()) {
                        Log.i("USERJOBS", jobSnapshot.toString());
                        usersJobs.put(jobSnapshot.getKey(),(List<String>)jobSnapshot.getValue());
                    }
                 counter++;
                checkIfNeedToStartActivity();

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }

    public void addValueEventListnerToTurnToEmployer(String phoneNumber){
        mDatabase.child("turned-to-employer").child(phoneNumber).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null)
                    allUserTurnedToEmployer = new HashMap<>();
                else
                    for (DataSnapshot jobSnapshot : dataSnapshot.getChildren()) {
                        Log.e(" snapshot", jobSnapshot.toString());
                        allUserTurnedToEmployer.put(jobSnapshot.getKey(),jobSnapshot.getValue(String.class));
                    }
                counter++;
                checkIfNeedToStartActivity();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }

    public void addValueEventListnerToSmartAgent(String phoneNumber) {
        mDatabase.child("smart-agent").child(phoneNumber).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null)
                    allSmartAgentJobs = new HashMap<>();
                else
                    for (DataSnapshot jobSnapshot : dataSnapshot.getChildren()) {
                        Log.e(" snapshot", jobSnapshot.toString());
                        allSmartAgentJobs.put(jobSnapshot.getKey(),jobSnapshot.getValue(String.class));
                    }
                counter++;
                checkIfNeedToStartActivity();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }

    public void writeNewUser(String userId, User user) {
        mDatabase.child("users").child(userId).setValue(user);
    }

    //זה מכניס את הגוב גם לטבלה של הגובים וגם לטבלה של הגובים שהמשתמש הזה פרסם
    public void WriteNewJob(Job job, String userId, Context context) {
//        String key = mDatabase.child("jobs").push().getKey();
//        job.setUid(key);
//        Map<String, Object> jobValues = job.toMap();
//        Map<String, Object> childUpdates = new HashMap<>();
//        childUpdates.put("/jobs/" + key, jobValues);
//        childUpdates.put("/user-jobs/" + userId + "/" + key, jobValues);
//        mDatabase.updateChildren(childUpdates);
        String key=mDatabase.child("jobs").push().getKey();//jobId
        job.setUid(key);
        mDatabase.child("jobs").child(key).setValue(job);
        List<String> userjobsid=new ArrayList<>();
        if(usersJobs.containsKey(userId)) {
             userjobsid = usersJobs.get(userId);
        }
        userjobsid.add(key);
        mDatabase.child("user-jobs").child(userId).setValue(userjobsid);
        addNewIndex(key,userId);
        //todo: create notification
        boolean result=needTocreateNotification(job,context);
        Log.i("tom","create notification: "+result);
        if(result) {
            createNotification(job, context);
            AddToSmartAgentJobs(job.getUid(),userId);
        }
    }

    public void createNotification(Job job, Context context) {
        //todo: important!!!! ask shay why when i press the back button or the X this is go out from the app
        String contentText=job.getCompany_name()+" מחפשת "+ProfessionLab.getInstance().
                getProfession(job.getProffesion(),context);
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.jobim_icon)
                        .setContentTitle(context.getString(R.string.jobim))
                        .setContentText(contentText)
                        .setAutoCancel(true)
                        .setSound(uri);

        // Creates an explicit intent for an Activity in your app

                Intent resultIntent = new Intent(context,JobItemExpendedActivity.class);
                resultIntent.putExtra("job",job);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(JobItemExpendedActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(resultIntent);
                PendingIntent resultPendingIntent =
                        stackBuilder.getPendingIntent(
                                0,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );
                mBuilder.setContentIntent(resultPendingIntent);
                NotificationManager mNotificationManager =
                        (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
                mNotificationManager.notify(1234, mBuilder.build());
    }

    public boolean needTocreateNotification(Job job,Context context) {
        NotificationSettings notificationSettings=NotificationSettings.getInstance();
        boolean isOn=notificationSettings.isOn();
        Log.i("tom",isOn+":ison");
        List<Integer> selectedJobsPosition=notificationSettings.getSelectedJobs();
        LatLng selectedLocation=notificationSettings.getSelectedLocation();
        int selectedRadius=notificationSettings.getSelectedRadius();
        if(!isOn)
            return false;
        if(selectedJobsPosition!=null && selectedLocation!=null && selectedRadius!=-1){
           if(!selectedJobsPosition.contains(job.getProffesion()))
                    return false;
            JobimLocation jobimLocation=JobimLocation.getInstance(context);
            int distanceInMeters= jobimLocation.getDestination(selectedLocation,
                    jobimLocation.getLocationLatLngFromAddress(job.getJob_location()));
            int distanceInKilometer=distanceInMeters/1000;
            if(selectedRadius<distanceInKilometer)
                return false;
            Log.i("tom",distanceInKilometer+"");

        }
        return true;
    }

    public List<Job> getAllJobs() {
        return allJobs;
    }

    public void WriteNewJobToRemove(String jobId, String userId) {
        allRemovedJobs.add(jobId);
        mDatabase.child("removed-jobs").child(userId).setValue(allRemovedJobs);
    }

    public void AddToMyFavoriteJobs(String jobId, String userId) {
        allFavoriteJobs.add(jobId);
        mDatabase.child("Favorite-jobs").child(userId).setValue(allFavoriteJobs);
    }

    public void AddToTurnedToEmployer(Job job, String userId) {
        DateFormat df = new SimpleDateFormat("dd/MM/yy");
        Calendar calobj = Calendar.getInstance();
        String date=df.format(calobj.getTime());
        Log.e("tom","Date: "+date);
        mDatabase.child("turned-to-employer").child(userId).child(job.getUid()).setValue(date);
        job.setTurnTo(true);
        job.setDate(date);
        UpdateChange(job,userId);

    }

    public void DeleteFromMyFavoriteJobs(String jobId, String userId) {
        allFavoriteJobs.remove(jobId);
        mDatabase.child("Favorite-jobs").child(userId).setValue(allFavoriteJobs);
    }

    public void UpdateChange(Job job, String userId) {
        Map<String, Object> jobValues = job.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/jobs/" + job.getUid(), jobValues);
        mDatabase.updateChildren(childUpdates);
    }

    public void addNewIndex(String jobId, String userId) {
        mDatabase.child("indexes").child(jobId).setValue(userId);
    }

    public String getAdvertiserUserId(String jobid) {
        String value="";
            if(indexes.containsKey(jobid))
                value=indexes.get(jobid);
        return value;
    }

    public List<String> getAdvertiserAllJobsId(String userId){
        List<String> jobsId=new ArrayList<>();
            if(usersJobs.containsKey(userId))
                jobsId=usersJobs.get(userId);
        return jobsId;
    }

    public List<Job> getAdvertiserAllJobs(List<String> jobsId){
        List<Job> jobs=new ArrayList<>();
        for (String jobId:jobsId) {
            for (Job item: allJobs)
                if(item.getUid().equals(jobId)) {
                    jobs.add(item);
                    break;
                }
        }
        return jobs;
    }

    public List<Job> getAllFavoriteJobs() {
        List<Job> jobs = new ArrayList<>();
        List<Job> allJobsToShow=getAllJobsToShow();
        for (String jobId : allFavoriteJobs) {
            for (Job item : allJobsToShow)
                if (item.getUid().equals(jobId)) {
                    jobs.add(item);
                    break;
                }
        }

        return jobs;
    }

    public List<Job> getAllRemovedJobs() {
        List<Job> jobs = new ArrayList<>();
        for (String jobId : allRemovedJobs) {
            for (Job item : allJobs)
                if (item.getUid().equals(jobId)) {
                    jobs.add(item);
                    break;
                }
        }
        Log.i("tom","allremovedJobs"+allRemovedJobs.size()+"");
        Log.i("tom","allremovedJobs"+allRemovedJobs.toString());
        Log.i("tom","jobs"+jobs.size()+"");

        return jobs;
    }

    public List<Job> getAllJobsToShow() {
        List<Job> result=allJobs;
        List<Job> removedJobs=getAllRemovedJobs();

        if(allJobs.size()!=0 && removedJobs.size()==0)
            return allJobs;

        if(allJobs.size()!=0 && removedJobs.size()!=0) {
           for(Job removedJob: removedJobs)
               if(result.contains(removedJob))
                   result.remove(removedJob);
        }
        Log.i("tom",result.size()+"");
        return result;
    }

    public List<Job> getAllTurnedToEmployerJobs(){
        List<Job> result=new ArrayList<>();
        for(Job job:allJobs)
            if(allUserTurnedToEmployer.containsKey(job.getUid()))
                result.add(job);
        return result;
    }

    public void AddToSmartAgentJobs(String jobId, String userId) {
        DateFormat df = new SimpleDateFormat("dd/MM/yy");
        Calendar calobj = Calendar.getInstance();
        String date=df.format(calobj.getTime());
        Log.e("tom","Date: "+date);
        mDatabase.child("smart-agent").child(userId).child(jobId).setValue(date);
    }

    public List<Job> getAllSmartAgentJobs() {
        List<Job> allJobsToShow=getAllJobsToShow();
        List<Job> result=new ArrayList<>();
        for(Job job:allJobsToShow)
            if(allSmartAgentJobs.containsKey(job.getUid()))
                result.add(job);
        return result;
    }

    public ArrayList<String> getAllSmartAgentJobsDate() {
        return (ArrayList<String>) new ArrayList(allSmartAgentJobs.values());
    }

}