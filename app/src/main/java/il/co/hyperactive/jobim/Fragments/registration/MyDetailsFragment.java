package il.co.hyperactive.jobim.Fragments.registration;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import il.co.hyperactive.jobim.Activitys.EditActivity;
import il.co.hyperactive.jobim.Model.SharedPreferencesSingleton;
import il.co.hyperactive.jobim.Model.User;
import il.co.hyperactive.jobim.R;

public class MyDetailsFragment extends Fragment implements View.OnClickListener{
    TextView tv_name_lastname,tv_city,tv_year,tv_email;
    CircleImageView profil_photo;
    User user;
    View rootView;
    public MyDetailsFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.fragment_my_details, container, false);
        init();
        return rootView;
    }
    private void init() {
        tv_name_lastname=(TextView)rootView.findViewById(R.id.tv_myDetails_first_last_name);
        tv_city=(TextView)rootView.findViewById(R.id.tv_myDetails_city);
        tv_year=(TextView)rootView.findViewById(R.id.tv_myDetails_year);
        tv_email=(TextView)rootView.findViewById(R.id.tv_myDetails_email);
        profil_photo=(CircleImageView) rootView.findViewById(R.id.iv_myDetails_profilpic);
        resizeAllTvIcons();

        profil_photo.setOnClickListener(this);
        tv_name_lastname.setOnClickListener(this);
        tv_city.setOnClickListener(this);
        tv_year.setOnClickListener(this);
        tv_email.setOnClickListener(this);
    }

    private void resizeAllTvIcons() {
        Drawable image=getResources().getDrawable(R.drawable.ic_perm_identity_white_24dp);
        Drawable imageResize=resize(image);
        tv_name_lastname.setCompoundDrawablesRelativeWithIntrinsicBounds(imageResize,null,null,null);
        image=getResources().getDrawable(R.drawable.ic_date_range_white_24dp);
        imageResize=resize(image);
        tv_year.setCompoundDrawablesRelativeWithIntrinsicBounds(imageResize,null,null,null);
        image=getResources().getDrawable(R.drawable.ic_mail_outline_white_24dp);
        imageResize=resize(image);
        tv_email.setCompoundDrawablesRelativeWithIntrinsicBounds(imageResize,null,null,null);

    }

    private Drawable resize(Drawable image) {
        Bitmap b = ((BitmapDrawable)image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, 70, 70, false);
        return new BitmapDrawable(getResources(), bitmapResized);
    }
    @Override
    public void onClick(View v) {
        Intent intent=new Intent(getActivity(),EditActivity.class);
        switch (v.getId()){
            case R.id.iv_myDetails_profilpic:
                intent.putExtra("FragmentName",NameLastNamePictureFragment.FRAGMENT_NAME);
                intent.putExtra("profil_pic",true);
                break;
            case R.id.tv_myDetails_first_last_name:
                intent.putExtra("FragmentName",NameLastNamePictureFragment.FRAGMENT_NAME);
                intent.putExtra("profil_pic",false);
                break;
            case R.id.tv_myDetails_city:
                intent.putExtra("FragmentName", CityFragment.FRAGMENT_NAME);
                break;
            case R.id.tv_myDetails_year:
                intent.putExtra("FragmentName", YearsOfBornFragment.FRAGMENT_NAME);
                break;
            case R.id.tv_myDetails_email:
                intent.putExtra("FragmentName", EmailFragment.FRAGMENT_NAME);
                break;
        }
        startActivity(intent);
    }


    @Override
    public void onResume() {
        super.onResume();
        user= SharedPreferencesSingleton.getInstance(getActivity()).readUser();
        if(user!=null ){
            Log.e("read user", user.toString());
            tv_name_lastname.setText(user.name + " " + user.lastname);
            if (!user.photoPath.equals("empty")) {
                Log.i("photoPath", user.photoPath);
                profil_photo.setImageURI(null);
                profil_photo.setImageURI(Uri.parse(user.photoPath));
            }
            tv_city.setText(user.city);
            tv_year.setText(user.year);
            tv_email.setText(user.email);
        }
    }
}
