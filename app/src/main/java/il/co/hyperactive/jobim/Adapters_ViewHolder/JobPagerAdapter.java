package il.co.hyperactive.jobim.Adapters_ViewHolder;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import il.co.hyperactive.jobim.Fragments.findJobs.JobItemContactFragment;
import il.co.hyperactive.jobim.Fragments.findJobs.JobItemDeleteFragment;
import il.co.hyperactive.jobim.Fragments.findJobs.JobItemIncapsulatedFragment;
import il.co.hyperactive.jobim.Fragments.findJobs.JobListFragment;
import il.co.hyperactive.jobim.Model.Job;

public  class JobPagerAdapter extends FragmentStatePagerAdapter {
    private static int NUM_ITEMS = 3;
    private List<Fragment> fragments;
    public Job job;
    public JobListFragment jobListFragment;
    public int position;

    public JobPagerAdapter(FragmentManager fragmentManager, Job job, JobListFragment jobListFragment,
                           int position) {
        super(fragmentManager);
        this.job=job;
        this.jobListFragment=jobListFragment;
        this.position=position;

        fragments=new ArrayList<>(NUM_ITEMS);
        fragments.add(JobItemContactFragment.newInstance(job,true));
        fragments.add(JobItemIncapsulatedFragment.newInstance(job,true,position));
        fragments.add(JobItemDeleteFragment.newInstance(job,position));

        Log.e("tom","jobpagerAdapter,position: "+position);
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        Log.e("current fragment",fragments.get(position).toString()+" position="+position);
        return fragments.get(position);
    }


    //this is called when notifyDataSetChanged() is called
    @Override
    public int getItemPosition(Object object) {
        // refresh all fragments when data set changed
        return JobPagerAdapter.POSITION_NONE;
    }



}

