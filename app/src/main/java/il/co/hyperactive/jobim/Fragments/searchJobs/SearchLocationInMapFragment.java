package il.co.hyperactive.jobim.Fragments.searchJobs;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import il.co.hyperactive.jobim.Model.JobimLocation;
import il.co.hyperactive.jobim.Model.SearchJobSingleton;
import il.co.hyperactive.jobim.R;

public class SearchLocationInMapFragment extends Fragment implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener, TextView.OnEditorActionListener {

    private MapView mMapView;
    private GoogleMap googleMap;
    JobimLocation locationSingleton;
    private EditText et_location;
    private LatLng selectedLocation;
    SearchJobSingleton searchJobSingleton;
    public SearchLocationInMapFragment(){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationSingleton=JobimLocation.getInstance(getContext());
        searchJobSingleton=SearchJobSingleton.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_search_location_in_map, container, false);
        selectedLocation=searchJobSingleton.getSelectedLocation();
        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        et_location=(EditText)rootView.findViewById(R.id.jon_location_et_search);

        et_location.setOnEditorActionListener(this);

        mMapView.onCreate(savedInstanceState);
        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMapView.getMapAsync(this);

        return rootView;
    }

    private Bitmap resize(Drawable image) {
        Bitmap b = ((BitmapDrawable)image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, 280, 180, false);
        return bitmapResized;
    }
    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
        searchJobSingleton.setSelectedLocation(getSelectedLocation());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap mMap) {
        googleMap = mMap;
        googleMap.setMyLocationEnabled(true); //Gets the status of the my-location layer.
        LatLng currentLocation = locationSingleton.getCurrentLocationLatLng();
        if(selectedLocation==null)
            addMarker(currentLocation);
        else{
            addMarker(selectedLocation);
            String address=locationSingleton.getAddressByLatLng(selectedLocation);
            et_location.setText(address);
        }

        CameraPosition cameraPosition = new CameraPosition.Builder().target(currentLocation).zoom(12).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        googleMap.setOnMapClickListener(this);



    }

    private void addMarker(LatLng latLng) {
        MarkerOptions markerOptions=new MarkerOptions();
        markerOptions.position(latLng);
        Drawable drawable=getResources().getDrawable(R.drawable.ic_location_orangee);
        Bitmap bitmap=resize(drawable);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
        googleMap.addMarker(markerOptions);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        googleMap.clear();
        addMarker(latLng);
        String address=locationSingleton.getAddressByLatLng(latLng);
        et_location.setText(address);
        selectedLocation=latLng;
        searchJobSingleton.setSelectedLocation(getSelectedLocation());

    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        boolean handled = false;
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            String address=et_location.getText()+"";
            LatLng latLng=locationSingleton.getLocationLatLngFromAddress(address);
            if(latLng!=null) {
                googleMap.clear();
                addMarker(latLng);
                selectedLocation=latLng;
                searchJobSingleton.setSelectedLocation(getSelectedLocation());
            }
            //handled = true;
        }
        return handled;
    }

    public LatLng getSelectedLocation(){
         return selectedLocation;
    }
}
