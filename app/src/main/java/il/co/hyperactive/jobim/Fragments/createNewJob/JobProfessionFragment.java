package il.co.hyperactive.jobim.Fragments.createNewJob;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.util.List;

import il.co.hyperactive.jobim.Adapters_ViewHolder.ProfessionAdapter;
import il.co.hyperactive.jobim.Fragments.registration.RegistrationInputFragment;
import il.co.hyperactive.jobim.Model.Profession;
import il.co.hyperactive.jobim.Model.ProfessionLab;
import il.co.hyperactive.jobim.R;

public class JobProfessionFragment extends RegistrationInputFragment {
    private RecyclerView mProfessionRecyclerView;
    private ProfessionAdapter mAdapter;
    List<Profession> professions;
    public JobProfessionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview= inflater.inflate(R.layout.fragment_job_profession, container, false);
        mProfessionRecyclerView = (RecyclerView) rootview.findViewById(R.id.profession_recycler_view);
        mProfessionRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
         mProfessionRecyclerView.getContext(), LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.line_seperator));
        mProfessionRecyclerView.addItemDecoration(dividerItemDecoration);
        updateUI();

        return rootview;

    }

    private void updateUI() {
        ProfessionLab professionLab = ProfessionLab.getInstance();
        professions = professionLab.getProfessions();

        if (mAdapter == null) {
            mAdapter = new ProfessionAdapter(professions,getActivity());
            mProfessionRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public String getParams() {
        String result="error";
        if(mAdapter.mCheckedPosition!=-1)
            result=mAdapter.mCheckedPosition+"";
       else
            Toast.makeText(getActivity(),"אתה חייב לבחור גו'ב",Toast.LENGTH_SHORT).show();

       return result;
    }


}