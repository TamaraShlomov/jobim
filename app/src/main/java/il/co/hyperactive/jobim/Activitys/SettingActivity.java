package il.co.hyperactive.jobim.Activitys;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import il.co.hyperactive.jobim.Fragments.other.LocationAndRadiusFragment;
import il.co.hyperactive.jobim.Fragments.searchJobs.JobProfessionMultiChoiseFragment;
import il.co.hyperactive.jobim.Model.NotificationSettings;
import il.co.hyperactive.jobim.Model.SearchJobSingleton;
import il.co.hyperactive.jobim.Model.SharedPreferencesSingleton;
import il.co.hyperactive.jobim.R;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar myToolbar;
    private TextView save;
    private FragmentManager fm;
    private Fragment fragment;
    private int lastPosition;
    int currentPosition;
    private Button btn_job;
    private Button btn_location;
    private SwitchCompat switc;
    final NotificationSettings notificationSettings=NotificationSettings.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        initToolBar();
        initViews();
    }

    private void initToolBar() {
        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        TextView tv_center=(TextView)myToolbar.findViewById(R.id.tv_toolbar_center);
        tv_center.setText(R.string.setting);
        tv_center.setVisibility(View.VISIBLE);
        save=(TextView)myToolbar.findViewById(R.id.tv_action_left);
        save.setText(R.string.save);
        save.setOnClickListener(this);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.right_side_toolbar, menu);
        MenuItem backItem = menu.findItem(R.id.menu_item_back);
        backItem.setVisible(true);
        backItem.setVisible(true);
        backItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Log.i("ok_cancel_action","cancel");
                SearchJobSingleton.getInstance().cleanAll(); // not sure if it needed
                 finish();
                return false;
            }
        });

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_back:
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_action_left: //save action
                onPressOk();
                return;
            case R.id.fragment_setting_btn_job:
                fragment =  JobProfessionMultiChoiseFragment.newInstance(false);
                break;
            case R.id.fragment_setting_btn_location:
                fragment=new LocationAndRadiusFragment();
                break;
        }
        if(v.getId()!=R.id.tv_action_left) {
            currentPosition = v.getId();
            changeColor();
            lastPosition = v.getId();
            fm.beginTransaction()
                    .replace(R.id.fragment_setting_continer, fragment)
                    .commit();
        }

    }

    private void initViews() {
        switc=(SwitchCompat) findViewById(R.id.switch_compat);
        if(notificationSettings.isOn())
            switc.setChecked(true);
        fm = getSupportFragmentManager();
        fragment = fm.findFragmentById(R.id.fragment_setting_continer);

        lastPosition=R.id.fragment_setting_btn_job;
        btn_job=(Button)findViewById(R.id.fragment_setting_btn_job);
        btn_location=(Button)findViewById(R.id.fragment_setting_btn_location);


        btn_job.setOnClickListener(this);
        btn_location.setOnClickListener(this);


        if(fragment==null) {
            btn_job.setBackgroundResource(R.drawable.background_orange_with_radius);
            btn_job.setTextColor(getResources().getColor(android.R.color.white));
            fragment = new JobProfessionMultiChoiseFragment();
            fm.beginTransaction()
                    .add(R.id.fragment_setting_continer, fragment)
                    .commit();
        }
        else
            btn_job.performClick();
    }

    private void changeColor() {
        Button btn=(Button)findViewById(lastPosition);
        btn.setBackgroundResource(android.R.color.transparent);
        btn.setTextColor(getResources().getColor(android.R.color.black));

        btn=(Button)findViewById(currentPosition);
        btn.setBackgroundResource(R.drawable.background_orange_with_radius);
        btn.setTextColor(getResources().getColor(android.R.color.white));
    }

    private void onPressOk() {

        List<Integer> selectedjobPositions=notificationSettings.getSelectedJobs();
        LatLng selectedLocation=notificationSettings.getSelectedLocation();
        int selectedRadius=notificationSettings.getSelectedRadius();
        notificationSettings.setOn(switc.isChecked());
        Log.e("isOn",switc.isChecked()+"");
        SharedPreferencesSingleton.getInstance(this).WriteNotificationFilter(notificationSettings);
        if(switc.isChecked())
            if (selectedjobPositions == null && selectedRadius == -1 && selectedLocation == null)
                Toast.makeText(this, "לא בחרת סינונים,ולכן לא תקבל התראות", Toast.LENGTH_SHORT);

       finish();
    }
}
