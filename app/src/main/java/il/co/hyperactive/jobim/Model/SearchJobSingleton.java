package il.co.hyperactive.jobim.Model;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import il.co.hyperactive.jobim.R;

/**
 * Created by Tom on 22/06/2017.
 */

public class SearchJobSingleton {
    private static SearchJobSingleton searchJobSingleton;
    List<Integer> selectedJobs;
    LatLng selectedLocation;
    int selectedCompanyPosition;
    private SearchJobSingleton(){
        selectedJobs=null;
        selectedLocation=null;
        selectedCompanyPosition=-1;
    }

    public static SearchJobSingleton getInstance(){
        if(searchJobSingleton==null)
            searchJobSingleton=new SearchJobSingleton();
        return searchJobSingleton;
    }

    public List<Integer> getSelectedJobs(){
        return selectedJobs;
    }
    public LatLng getSelectedLocation(){
        return selectedLocation;
    }
    public int getSelectedCompanyPosition(){
        return selectedCompanyPosition;
    }

    public void setSelectedJobs(List<Integer> selectedJobs) {
        this.selectedJobs = selectedJobs;
    }

    public void setSelectedLocation(LatLng selectedLocation) {
        this.selectedLocation = selectedLocation;
    }

    public void setSelectedCompanyPosition(int selectedCompanyPosition) {
        this.selectedCompanyPosition = selectedCompanyPosition;
    }

    public void cleanAll(){
        selectedJobs=null;
        selectedLocation=null;
        selectedCompanyPosition=-1;
    }
}
