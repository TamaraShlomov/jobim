package il.co.hyperactive.jobim.Fragments.findJobs;

import android.Manifest;
import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import il.co.hyperactive.jobim.Model.Job;
import il.co.hyperactive.jobim.Model.JobimLocation;
import il.co.hyperactive.jobim.R;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kishan.askpermission.AskPermission;
import com.kishan.askpermission.ErrorCallback;
import com.kishan.askpermission.PermissionCallback;
import com.kishan.askpermission.PermissionInterface;

public class MapFragment extends Fragment implements PermissionCallback, ErrorCallback, OnMapReadyCallback, GoogleMap.OnMapClickListener {

    private MapView mMapView;
    private GoogleMap googleMap;
    Job job;
    JobimLocation locationSingletoon;
    static boolean firstClick=true;
    public MapFragment(){}

    public static MapFragment newInstance(Job job) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("job",job);

        MapFragment fragment = new MapFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            job =(Job)bundle.getSerializable("job");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new AskPermission.Builder(this)
                .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION)
                .setCallback(this)
                .setErrorCallback(this)
                .request(1);
        locationSingletoon= JobimLocation.getInstance(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        readBundle(getArguments());
        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onPermissionsGranted(int i) {}
    @Override
    public void onPermissionsDenied(int i) {}
    @Override
    public void onShowRationalDialog(PermissionInterface permissionInterface, int i) {}
    @Override
    public void onShowSettings(PermissionInterface permissionInterface, int i) {}

    private Bitmap resize(Drawable image) {
        Bitmap b = ((BitmapDrawable)image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, 100, 100, false);
        return bitmapResized;
    }
    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap mMap) {
            googleMap = mMap;
            googleMap.setMyLocationEnabled(false); //the blue spot

            // For dropping a marker at a point on the Map
            LatLng jobLocation= locationSingletoon.getLocationLatLngFromAddress(job.getJob_location());
            MarkerOptions markerOptions=new MarkerOptions();
            if(jobLocation!=null) {
                markerOptions.position(jobLocation);
                Drawable drawable=getResources().getDrawable(R.drawable.ic_location_orangee);
                Bitmap bitmap=resize(drawable);
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
             //   markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
                googleMap.addMarker(markerOptions);
            }
            /////////////
            LatLng currentLocation=locationSingletoon.getCurrentLocationLatLng();
            if(currentLocation!=null) {
                markerOptions.position(currentLocation);
                Drawable drawable=getResources().getDrawable(R.drawable.ic_location_indicator);
                Bitmap bitmap=resize(drawable);
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
               // markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
                googleMap.addMarker(markerOptions);
            }
            ////////////

            // For zooming automatically to the location of the marker
            CameraPosition cameraPosition = new CameraPosition.Builder().target(currentLocation).zoom(12).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            googleMap.setOnMapClickListener(this);

        }

    @Override
    public void onMapClick(LatLng latLng) {
        Log.i("onclick","done");
        ViewGroup.LayoutParams params = mMapView.getLayoutParams();

        if(firstClick)
            params.height = 800;
        else
            params.height = 500;

        firstClick=!firstClick;
        mMapView.requestLayout();

    }
}
