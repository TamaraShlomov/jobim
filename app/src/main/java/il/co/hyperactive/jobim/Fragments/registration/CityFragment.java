package il.co.hyperactive.jobim.Fragments.registration;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import il.co.hyperactive.jobim.Model.NotificationSettings;
import il.co.hyperactive.jobim.Model.SharedPreferencesSingleton;
import il.co.hyperactive.jobim.Model.User;
import il.co.hyperactive.jobim.R;
import il.co.hyperactive.jobim.Tasks.CityTask;

/**
 * Created by Tom on 08/05/2017.
 */

public class CityFragment extends RegistrationInputFragment {
    public static AutoCompleteTextView atvPlaces;
    CityTask placesTask;
    String selectedCity;
    public static final String FRAGMENT_NAME="CityFragment";
    public CityFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_city, container, false);
        atvPlaces = (AutoCompleteTextView) rootView.findViewById(R.id.autocomplete_cities);
        selectedCity=atvPlaces.getText().toString();
        Log.e("cityFragment","yes");
        if(SharedPreferencesSingleton.getInstance(getActivity()).readUser()!=null){
            User user=SharedPreferencesSingleton.getInstance(getActivity()).readUser();
            atvPlaces.setText(user.city);
            selectedCity=user.city;
        }

        //find my current city by location//
        //in the future add code
        ////
        atvPlaces.setThreshold(2);
        atvPlaces.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.i("tom", "ontextchanged");
                if ((s.toString()).replace(" ", "").length() > 1) {
                    s = s.toString().replace(" ", "");
                    placesTask = new CityTask(getActivity());
                    placesTask.execute(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        atvPlaces.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("tom", "onItemClick");
                InputMethodManager mgr = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(atvPlaces.getWindowToken(), 0);
                TextView itemselected=(TextView)view;
                Log.e("tom","item selected "+itemselected.getText().toString());
                 selectedCity=itemselected.getText().toString();

            }
        });
        return rootView;
    }

    @Override
    public String getParams() {
        return selectedCity;
    }
}
