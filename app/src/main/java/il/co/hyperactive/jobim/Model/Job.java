package il.co.hyperactive.jobim.Model;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tom on 01/06/2017.
 */

public class Job implements Serializable{
    private String company_name;
    private String brunch;
    private int proffesion;
    private  String job_title;
    private String job_discription;
    private String job_location;
    private String image_path;
    private boolean for_teens;
    private  String filter_question;
    private String sms;
    private String call;
    private String mail;
    private boolean star;
    private String uid;
    private boolean turnTo;
    private String date;

    public Job(){}

    public Job(String company_name, String brunch, int proffesion, String job_title,
               String job_discription, String job_location, String image_path,
               boolean for_teens, String filter_question, String sms, String call, String mail) {
        this.company_name = company_name;
        this.brunch = brunch;
        this.proffesion = proffesion;
        this.job_title = job_title;
        this.job_discription = job_discription;
        this.job_location = job_location;
        this.image_path = image_path;
        this.for_teens = for_teens;
        this.filter_question = filter_question;
        this.sms = sms;
        this.call = call;
        this.mail = mail;
        star=false;
        turnTo=false;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getBrunch() {
        return brunch;
    }

    public void setBrunch(String brunch) {
        this.brunch = brunch;
    }

    public int getProffesion() {
        return proffesion;
    }

    public void setProffesion(int proffesion) {
        this.proffesion = proffesion;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public String getJob_discription() {
        return job_discription;
    }

    public void setJob_discription(String job_discription) {
        this.job_discription = job_discription;
    }

    public String getJob_location() {
        return job_location;
    }

    public void setJob_location(String job_location) {
        this.job_location = job_location;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public boolean isFor_teens() {
        return for_teens;
    }

    public void setFor_teens(boolean for_teens) {
        this.for_teens = for_teens;
    }

    public String getFilter_question() {
        return filter_question;
    }

    public void setFilter_question(String filter_question) {
        this.filter_question = filter_question;
    }

    public String getSms() {
        return sms;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    public String getCall() {
        return call;
    }

    public void setCall(String call) {
        this.call = call;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public boolean isStar() {
        return star;
    }

    public void setStar(boolean star) {
        this.star = star;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public boolean isTurnTo() {
        return turnTo;
    }

    public void setTurnTo(boolean turnTo) {
        this.turnTo = turnTo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Job{" +
                "company_name='" + company_name + '\'' +
                ", brunch='" + brunch + '\'' +
                ", proffesion=" + proffesion +
                ", job_title='" + job_title + '\'' +
                ", job_discription='" + job_discription + '\'' +
                ", job_location='" + job_location + '\'' +
                ", image_path='" + image_path + '\'' +
                ", for_teens=" + for_teens +
                ", filter_question='" + filter_question + '\'' +
                ", sms='" + sms + '\'' +
                ", call='" + call + '\'' +
                ", mail='" + mail + '\'' +
                ", star=" + star +
                ", uid='" + uid + '\'' +
                '}';
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();

        result.put("company_name", company_name);
        result.put("brunch", brunch);
        result.put("proffesion", proffesion);
        result.put("job_title", job_title);
        result.put("job_discription", job_discription);
        result.put("job_location",job_location);
        result.put("image_path",image_path);
        result.put("for_teens",for_teens);
        result.put("filter_question",filter_question);
        result.put("sms",sms);
        result.put("call",call);
        result.put("mail",mail);
        result.put("star",star);
        result.put("uid", uid);
        result.put("turnTo",turnTo);
        result.put("date",date);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        return this.getUid().equals(((Job)obj).getUid());
    }
}
