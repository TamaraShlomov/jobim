package il.co.hyperactive.jobim.Fragments.findJobs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import il.co.hyperactive.jobim.Activitys.JobItemExpendedActivity;
import il.co.hyperactive.jobim.Activitys.NavigationActivity;
import il.co.hyperactive.jobim.Interface.delete;
import il.co.hyperactive.jobim.Model.Job;
import il.co.hyperactive.jobim.Model.JobimLocation;
import il.co.hyperactive.jobim.Model.ProfessionLab;
import il.co.hyperactive.jobim.R;

/**
 * Created by Tom on 05/06/2017.
 */

public class JobItemIncapsulatedFragment extends Fragment implements View.OnClickListener {
    Job job;
    TextView tv_title,tv_discription,tv_job_location,tv_distance_from_current_location;
    ImageView iv_job_icon;
    View rootView;
    LinearLayout locationView;
    boolean locationview_visible;
    private TextView job_destination_tv;
    JobimLocation locationSingleton;
    int position=0;
    public static final int PICK_JOB_TO_DELETE_REQUEST = 1;  // The request code

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
    public static JobItemIncapsulatedFragment newInstance(Job job,boolean visible,int position) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("job",job);
        bundle.putBoolean("visible",visible);
        bundle.putInt("position",position);
        JobItemIncapsulatedFragment fragment = new JobItemIncapsulatedFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            job =(Job)bundle.getSerializable("job");
            locationview_visible=bundle.getBoolean("visible");
            position=bundle.getInt("position");

        }
        Log.i("bundle",bundle.toString());
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView=inflater.inflate(R.layout.fragment_job_item_incapsulated,container,false);
        readBundle(getArguments());
        Log.i("tom",this.getContext().getClass().toString());
        locationSingleton=JobimLocation.getInstance(getContext());
        init();
       return rootView;
    }

    private void init() {
        tv_title=(TextView)rootView.findViewById(R.id.job_item_textview_title);
        tv_discription=(TextView)rootView.findViewById(R.id.job_item_textview_discription);
        tv_job_location=(TextView)rootView.findViewById(R.id.job_item_textview_location);
        tv_distance_from_current_location=(TextView)rootView.findViewById(R.id.job_item_textview_distance_from_my_current_location);
        iv_job_icon=(ImageView)rootView.findViewById(R.id.job_item_imageView);
        locationView=(LinearLayout) rootView.findViewById(R.id.incap_location);
        if(!locationview_visible) {
            locationView.setVisibility(View.GONE);
            rootView.setEnabled(false);
        }
        job_destination_tv=(TextView)rootView.findViewById(R.id.job_item_textview_distance_from_my_current_location);

        if(job!=null) {
            String profession_text=getString(ProfessionLab.getInstance().getProfessions().
                    get(job.getProffesion()).getString_id_textView());
            int profession_icon=ProfessionLab.getInstance().getProfessions().
                    get(job.getProffesion()).getResuorce_id_imageView();
            int profession_color=ProfessionLab.getInstance().getProfessions().
                    get(job.getProffesion()).getResuorce_id_color();
            tv_title.setText(String.format("%s מחפשת %s", job.getCompany_name(), profession_text));
            tv_discription.setText(job.getJob_title());
            tv_job_location.setText(job.getJob_location());
            iv_job_icon.setImageResource(profession_icon);
        //    rootView.setBackgroundResource(profession_color);
            CardView cardView=(CardView) rootView.findViewById(R.id.card_view);
            cardView.setBackgroundResource(profession_color);

            int distanceInMeters=locationSingleton.getDestination(
                    locationSingleton.getLocationLatLngFromAddress(job.getJob_location()),
                    locationSingleton.getCurrentLocationLatLng());
            String distance=distanceInMeters>1000?distanceInMeters/1000+" קמ ":distanceInMeters+" מטר ";
            job_destination_tv.setText(distance);

            View view=rootView.findViewById(R.id.turned_to_employer_layout);
            if(job.isTurnTo() && locationview_visible){
                view.setVisibility(View.VISIBLE);
                ImageView icon=(ImageView) rootView.findViewById(R.id.turned_to_employer_image_view);
                EditText editText=(EditText)rootView.findViewById(R.id.turned_to_employer_text);
                Resources res = getResources();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    icon.setBackgroundTintList(res.getColorStateList(profession_color));
                }
                String text = res.getString(R.string.turned_to_employer_date,job.getDate());
                editText.setText(text);
            }
            else
                view.setVisibility(View.GONE);
        }
        rootView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent=new Intent(getActivity(),JobItemExpendedActivity.class);
        intent.putExtra("job",job);
        intent.putExtra("position",position);
        getActivity().startActivityForResult(intent,PICK_JOB_TO_DELETE_REQUEST);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


}
