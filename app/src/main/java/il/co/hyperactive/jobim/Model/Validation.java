package il.co.hyperactive.jobim.Model;

/**
 * Created by Tom on 15/05/2017.
 */

public class Validation {

    static public boolean isValidField(String fragmentName,String field){
        switch (fragmentName){
            case "NameLastName":
                return !((field.replaceAll(" ","")).equals(""));
            case "Email":
                if((field.replaceAll(" ","")).equals(""))
                    return false;
                if(!field.contains("@"))
                    return false;
                String[] emailchars=field.split("@");
                if(emailchars.length!=2)
                    return false;
                if(emailchars[0].contains(" ")||emailchars[1].contains(" "))
                    return false;

        }
        return true;

    }
}
