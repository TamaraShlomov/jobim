package il.co.hyperactive.jobim.Activitys;

import android.Manifest;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.List;

import il.co.hyperactive.jobim.Fragments.findJobs.JobItemContactFragment;
import il.co.hyperactive.jobim.Fragments.findJobs.JobItemIncapsulatedFragment;
import il.co.hyperactive.jobim.Fragments.findJobs.JobListFragment;
import il.co.hyperactive.jobim.Fragments.findJobs.MapFragment;
import il.co.hyperactive.jobim.Model.FireBaseSingleton;
import il.co.hyperactive.jobim.Model.Job;
import il.co.hyperactive.jobim.Model.JobimLocation;
import il.co.hyperactive.jobim.Model.ProfessionLab;
import il.co.hyperactive.jobim.Model.SharedPreferencesSingleton;
import il.co.hyperactive.jobim.R;

public class JobItemExpendedActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int PICK_CONTACT_REQUEST = 1;
    Job job;
    ImageButton removeAddFavorite_btn,moreJobs_btn,shareWithFirend_btn,remove_job_btn;
    TextView tv_star;
    FireBaseSingleton fireBaseSingleton;
    String phoneNumber;
    TextView job_location_tv,job_destination_tv;
    JobimLocation locationSingleton;
    Toolbar mToolBar;
    private View.OnClickListener onClickAlertDialogButton;
    private AlertDialog alertDialog;
    private JobListFragment jobListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_item_expended);
        locationSingleton=JobimLocation.getInstance(this);
        job=(Job)getIntent().getSerializableExtra("job");
        fireBaseSingleton=FireBaseSingleton.getInstance(this);
        SharedPreferencesSingleton sharedPreferencesSingleton=SharedPreferencesSingleton.
                getInstance(this);
        phoneNumber=sharedPreferencesSingleton.readPhoneNumber();

        mToolBar = (Toolbar) findViewById(R.id.job_item_expended_my_toolBar);
        mToolBar.setNavigationIcon(resize(getResources().getDrawable(R.drawable.expanded_close)));
        setSupportActionBar(mToolBar);
        getSupportActionBar().setTitle(null);

        initFragments();
        initViews();
    }

    private Drawable resize(Drawable image) {
        Bitmap b = ((BitmapDrawable)image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, 100, 100, false);
        return new BitmapDrawable(getResources(), bitmapResized);
    }

    private void initFragments() {
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment1 = fm.findFragmentById(R.id.incap_job_fragment);

        if (fragment1 == null) {
            fragment1 = JobItemIncapsulatedFragment.newInstance(job,false,-1);
            fm.beginTransaction()
                    .add(R.id.incap_job_fragment, fragment1)
                    .commit();
        }

        Fragment fragment2 = fm.findFragmentById(R.id.fragment_contact);

        if (fragment2 == null) {
            fragment2 = JobItemContactFragment.newInstance(job,false);
            fm.beginTransaction()
                    .add(R.id.fragment_contact, fragment2)
                    .commit();
        }

        Fragment mapFragment = fm.findFragmentById(R.id.fragment_map);

        if (mapFragment == null) {
            mapFragment = MapFragment.newInstance(job);
            fm.beginTransaction()
                    .add(R.id.fragment_map, mapFragment)
                    .commit();
        }
    }

    private void initViews(){
        removeAddFavorite_btn=(ImageButton)findViewById(R.id.remove_add_favorite_imageButton);
        moreJobs_btn=(ImageButton)findViewById(R.id.more_jobs_imageButton);
        shareWithFirend_btn=(ImageButton)findViewById(R.id.share_with_friend_imageButton);
        remove_job_btn=(ImageButton)findViewById(R.id.remove_job_imageButton);
        tv_star=(TextView)findViewById(R.id.tv_star);

        job_location_tv=(TextView)findViewById(R.id.job_item_textview_location);
        job_location_tv.setText(job.getJob_location());
        job_destination_tv=(TextView)findViewById(R.id.job_item_textview_distance_from_my_current_location);

        int distanceInMeters=locationSingleton.getDestination(
                locationSingleton.getLocationLatLngFromAddress(job.getJob_location()),
                locationSingleton.getCurrentLocationLatLng());
        String distance=distanceInMeters>1000?distanceInMeters/1000+" קמ ":distanceInMeters+" מטר ";
        job_destination_tv.setText(distance);
        if(job!=null && job.isStar()) {
            removeAddFavorite_btn.setBackgroundResource(R.drawable.ic_star_full);
            tv_star.setText(R.string.remove_job_from_favorite);
        }
        removeAddFavorite_btn.setOnClickListener(this);
        moreJobs_btn.setOnClickListener(this);
        shareWithFirend_btn.setOnClickListener(this);
        remove_job_btn.setOnClickListener(this);

        onClickAlertDialogButton = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickAlertDialogButton(v);
            }
        };
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.remove_add_favorite_imageButton:
                if(job.isStar()){
                    removeAddFavorite_btn.setBackgroundResource(R.drawable.ic_star);
                    tv_star.setText(R.string.add_to_favourites);
                    fireBaseSingleton.DeleteFromMyFavoriteJobs(job.getUid(),phoneNumber);
                }
                else{
                    removeAddFavorite_btn.setBackgroundResource(R.drawable.ic_star_full);
                    tv_star.setText(R.string.remove_job_from_favorite);
                    fireBaseSingleton.AddToMyFavoriteJobs(job.getUid(),phoneNumber);
                }
                job.setStar(!job.isStar());
                fireBaseSingleton.UpdateChange(job,phoneNumber);
                break;
            case R.id.more_jobs_imageButton:
                String userId=fireBaseSingleton.getAdvertiserUserId(job.getUid());
                List<String> allAdvertiserjobsid=fireBaseSingleton.getAdvertiserAllJobsId(userId);
                List<Job> allAdvertiserJobs=fireBaseSingleton.getAdvertiserAllJobs(allAdvertiserjobsid);
                Intent intent=new Intent(this,MoreJobsFromThisEmployerActivity.class);
                intent.putExtra("jobs",(Serializable)allAdvertiserJobs);
                startActivity(intent);
                break;
            case R.id.share_with_friend_imageButton:
                pickContact();
                break;
            case R.id.remove_job_imageButton:
                createAlertDialog();
                break;
        }
    }
    //Sends an SMS message to another device
    private void sendSMS(String phoneNumber, String message) {
        try {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.SEND_SMS},1);
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(phoneNumber, null, message, null, null);
            Toast.makeText(this, "SMS Sent!",
                    Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(this,"SMS faild, please try again later!",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void pickContact() {
        Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
        pickContactIntent.setType(Phone.CONTENT_TYPE); // Show user only contacts w/ phone numbers
        startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request it is that we're responding to
        if (requestCode == PICK_CONTACT_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // Get the URI that points to the selected contact
                Uri contactUri = data.getData();
                // We only need the NUMBER column, because there will be only one row in the result
                String[] projection = {Phone.NUMBER};

                // Perform the query on the contact to get the NUMBER column
                // We don't need a selection or sort order (there's only one result for the given URI)
                // CAUTION: The query() method should be called from a separate thread to avoid blocking
                // your app's UI thread. (For simplicity of the sample, this code doesn't do that.)
                // Consider using CursorLoader to perform the query.
                Cursor cursor = getContentResolver()
                        .query(contactUri, projection, null, null, null);
                cursor.moveToFirst();

                // Retrieve the phone number from the NUMBER column
                int column = cursor.getColumnIndex(Phone.NUMBER);
                String number = cursor.getString(column);
                Resources res = getResources();
                String username= String.format("%s %s", SharedPreferencesSingleton.getInstance(this).readUser().name,
                        SharedPreferencesSingleton.getInstance(this).readUser().lastname);
                String jobcompany=job.getCompany_name();
                String jobProfession=res.getString(ProfessionLab.getInstance().getProfessions().
                        get(job.getProffesion()).getString_id_textView());
                String message = res.getString(R.string.shared_with_friend_message, username, jobcompany,jobProfession);
                sendSMS(number,message);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.right_side_toolbar, menu);
        MenuItem overFlowItem = menu.findItem(R.id.overflow);
        overFlowItem.setVisible(true);
//        overFlowItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                finish();
//                return false;
//            }
//        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.overflow:
                return true;
            case android.R.id.home:
               // finish();
                NavUtils.navigateUpFromSameTask(this);

                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }    }

    public void createAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        View mView= getLayoutInflater().inflate(R.layout.custom_alert_dialog,null);
        TextView title,message;
        Button btn_right_cancel,btn_left_delete;
        title=(TextView)mView.findViewById(R.id.custom_alert_dialog_title);
        message=(TextView)mView.findViewById(R.id.custom_alert_dialog_message);
        btn_left_delete=(Button)mView.findViewById(R.id.custom_alert_dialog_btn_left);
        btn_right_cancel=(Button)mView.findViewById(R.id.custom_alert_dialog_btn_right);

        title.setText(R.string.delete_job);
        message.setText(R.string.delete_message);
        btn_left_delete.setText(R.string.delete_from_fid);
        btn_right_cancel.setText(R.string.cancel);

        btn_left_delete.setOnClickListener(onClickAlertDialogButton);
        btn_right_cancel.setOnClickListener(onClickAlertDialogButton);
        alertDialogBuilder.setView(mView);
        alertDialog=alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();
    }

    public void onClickAlertDialogButton(View view) {
        switch (view.getId()){
            case R.id.custom_alert_dialog_btn_right:
                Log.i("tom","cancel");
                alertDialog.cancel();
                break;
            case R.id.custom_alert_dialog_btn_left:
                //  continue with delete
                Log.i("tom","delete from fid");

                FireBaseSingleton.getInstance(this).WriteNewJobToRemove(job.getUid(),phoneNumber);
                Intent intent=new Intent(this,NavigationActivity.class);
                intent.putExtra("position",getIntent().getIntExtra("position",-1));
                setResult(RESULT_OK,intent);
                alertDialog.cancel(); //not sure if this line needed
                finish();
                break;
            default:
                Log.i("tom","default");
                break;
        }
    }


}
