package il.co.hyperactive.jobim.Tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import il.co.hyperactive.jobim.Interface.AsyncResponse;
import il.co.hyperactive.jobim.Model.AdressJSONParser;
import il.co.hyperactive.jobim.Model.DownloadUrl;

/**
 * Created by Tom on 24/05/2017.
 */

public class AdressTask extends AsyncTask<String,Void,List<String>> {

    private Context context;
    public AsyncResponse delegate = null;
    final String KEY="AIzaSyDxyRcRSVRVWVxSy3oZWuxm4hhksshRkYY";

    public AdressTask(Context context,AsyncResponse delegate) {
        this.context = context;
        this.delegate=delegate;
    }
    @Override
    protected List<String> doInBackground(String... address) {
        // For storing data from web service
        String response = "";
        String key ="key="+KEY;
        String input="input=";
        try {
            input+=URLEncoder.encode(address[0],"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        // place type to be searched
        String types = "types=address";
        String components="components=country:il";
        String language="language=iw";
        // Building the parameters to the web service
        String parameters = input+"&"+types+"&"+components+"&"+language+"&"+key;
        // Output format
        String output= "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/place/autocomplete/"+output+"?"+parameters;
        Log.i("tom","url : "+url);
        List<String> result=null;
        try{
            // Fetching the data from web service in background
            response = DownloadUrl.getResponse(url);
            result=new AdressJSONParser().parserJson(response);
        }catch(Exception e){
            Log.e("Background Task",e.toString());
        }
        return result;
    }

    @Override
    protected void onPostExecute(List<String> result) {
        super.onPostExecute(result);
        delegate.processFinish(result);
    }




}
