package il.co.hyperactive.jobim.Interface;

/**
 * Created by Tom on 20/06/2017.
 */

public interface Filter {
    void showSearchFilters();

    void returnToFindJobsFragment();
}
