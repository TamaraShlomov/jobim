package il.co.hyperactive.jobim.Fragments.other;

import android.app.AlertDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import il.co.hyperactive.jobim.Activitys.NavigationActivity;
import il.co.hyperactive.jobim.Adapters_ViewHolder.NotificationAdapter;
import il.co.hyperactive.jobim.Model.FireBaseSingleton;
import il.co.hyperactive.jobim.R;

public class NotificationFragment extends Fragment {

    ArrayList<String> smart_agent_dates ;
    private NotificationAdapter adapter;

    public NotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_notification, container, false);
        smart_agent_dates= FireBaseSingleton.getInstance(getActivity()).getAllSmartAgentJobsDate();
        RecyclerView mRecyclerView=(RecyclerView) rootView.findViewById(R.id.fragment_noti_recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.line_seperator));
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        if(smart_agent_dates!=null && smart_agent_dates.size()>0) {
            adapter = new NotificationAdapter(smart_agent_dates,getActivity());
            mRecyclerView.setAdapter(adapter);
        }

        return rootView;
    }
}
