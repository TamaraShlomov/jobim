package il.co.hyperactive.jobim.Activitys;


import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import il.co.hyperactive.jobim.Fragments.registration.CityFragment;
import il.co.hyperactive.jobim.Fragments.registration.EmailFragment;
import il.co.hyperactive.jobim.Fragments.registration.NameLastNamePictureFragment;
import il.co.hyperactive.jobim.Fragments.registration.RegistrationInputFragment;
import il.co.hyperactive.jobim.Fragments.registration.YearsOfBornFragment;
import il.co.hyperactive.jobim.Model.FireBaseSingleton;
import il.co.hyperactive.jobim.Model.SharedPreferencesSingleton;
import il.co.hyperactive.jobim.Model.User;
import il.co.hyperactive.jobim.R;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.PhoneNumber;

public class UserRegistrationActivity extends AppCompatActivity {
    private FragmentPagerAdapter mPagerAdapter;
    private ViewPager mViewPager;
    Toolbar myToolbar;

    String phoneNumber;
    String first_name;
    String last_name;
    String city;
    String year;
    String email;
    String imagePath;
    FireBaseSingleton fireBaseSingleton;
    SharedPreferencesSingleton sharedPreferencesSingleton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration);

        fireBaseSingleton=FireBaseSingleton.getInstance(this);
        sharedPreferencesSingleton=SharedPreferencesSingleton.getInstance(getBaseContext());

        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);

        // Create the adapter that will return a fragment for each section
         final RegistrationInputFragment[] mFragments = new RegistrationInputFragment[] {
                new NameLastNamePictureFragment(),
                new CityFragment(),
                new YearsOfBornFragment(),
                new EmailFragment()
        };
        mPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {

            @Override
            public RegistrationInputFragment getItem(int position) {
                return mFragments[position];
            }
            @Override
            public int getCount() {
                return mFragments.length;
            }
        };
        final int[] icons=new int[]{R.drawable.ic_perm_identity_white_24dp,R.drawable.ic_location_on_white_24dp,
                R.drawable.ic_date_range_white_24dp,R.drawable.ic_mail_outline_white_24dp};

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mPagerAdapter);
        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        //////////cancel the option to click on the tab////////
        LinearLayout tabStrip = ((LinearLayout)tabLayout.getChildAt(0));
        tabStrip.setEnabled(false);
        for(int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setClickable(false);
        }
        /////////////////

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            final TabLayout.Tab tab = tabLayout.getTabAt(i);
            RelativeLayout relativeLayout = (RelativeLayout)
                    LayoutInflater.from(this).inflate(R.layout.tab, tabLayout, false);
            ImageView imageview=(ImageView)relativeLayout.findViewById(R.id.btn_tab);
            imageview.setImageResource(icons[i]);
            if(i==0)
                relativeLayout.setBackground(getResources().getDrawable(R.drawable.tab_backround_orange));
            tab.setCustomView(relativeLayout);

        }
        //disable swiping
        mViewPager.beginFakeDrag();


        final TextView next=(TextView)myToolbar.findViewById(R.id.tv_action_left);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               hideKeyBoard(v);
               int selectedtab= tabLayout.getSelectedTabPosition();
                boolean haveError=false;
                switch (selectedtab){
                    case 0:
                        if(mFragments[0].getParams()!="error") {
                            String[] params = mFragments[0].getParams().split(",");
                            first_name = params[0];
                            last_name = params[1];
                            imagePath=params[2];
                            Log.e("fragmenyNameLastName", first_name + " ," + last_name);
                        }
                        else
                         haveError=true;
                        break;
                    case 1:
                        city=mFragments[1].getParams();
                        break;
                    case 2:
                        year=mFragments[2].getParams();
                        break;
                    case 3:
                        if(mFragments[3].getParams()!="error")
                           email= mFragments[3].getParams();
                        else
                            haveError=true;

                }
                if(!haveError) {
                    if (selectedtab + 1 == tabLayout.getTabCount()) {
                        User user=new User(first_name,last_name,city,year,email,imagePath);
                        fireBaseSingleton.writeNewUser(phoneNumber/*"0542151182"*/,user);
                        sharedPreferencesSingleton.writeUser(user);
                        Intent intent=new Intent(UserRegistrationActivity.this,NavigationActivity.class);
                        intent.putExtra("continer","MyDetailsFragment");
                        startActivity(intent);
                    } else {
                        if (selectedtab + 1 == 3)
                            next.setText(R.string.save);

                            tabLayout.getTabAt(selectedtab + 1).select();
                            tabLayout.getTabAt(selectedtab + 1).getCustomView().setBackground(getResources().getDrawable(R.drawable.tab_backround_orange));

                    }
                }
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.right_side_toolbar, menu);
        MenuItem exitItem = menu.findItem(R.id.menu_item_exit);
        exitItem.setVisible(true);
        exitItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                startActivity(new Intent(UserRegistrationActivity.this,SplashActivity.class));
                return false;
            }
        });

        return true;

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_exit:
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                Log.e("sucess",account.getId());
                final PhoneNumber number = account.getPhoneNumber();
                phoneNumber=number.toString();
                sharedPreferencesSingleton.WritePhoneNumber(phoneNumber);
            }
            @Override
            public void onError(final AccountKitError error) {
                Log.e("oneroor","on eroor");
            }
        });
    }
    private void hideKeyBoard(View v) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this,"you cant go back !!",Toast.LENGTH_SHORT).show();
    }

}
