package il.co.hyperactive.jobim.Adapters_ViewHolder;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import il.co.hyperactive.jobim.Fragments.findJobs.JobListFragment;
import il.co.hyperactive.jobim.Model.Job;
import il.co.hyperactive.jobim.R;
/**
 * Created by Tom on 04/06/2017.
 */

public class JobAdapter extends RecyclerView.Adapter<JobAdapter.JobHolder>  {

    private List<Job> mJobList = new ArrayList<>();
    private Context context;
    private FragmentManager fragmentManager;
    private JobListFragment jobListFragment;

    public JobAdapter(List<Job> mJobList, Context context, FragmentManager fragmentManager,
                      JobListFragment jobListFragment) {
        this.mJobList = mJobList;
        this.context=context;
        this.fragmentManager=fragmentManager;
        this.jobListFragment=jobListFragment;
    }

    @Override
    public JobHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.find_job_line_content, parent, false);
        return new JobHolder(view);
    }

    @Override
    public void onBindViewHolder(final JobHolder holder, int position) {
        configurePagerHolder(holder, position);
    }

    private void configurePagerHolder(JobHolder holder, int position) {
        Job job= mJobList.get(position);
        Log.i("tom confPagerHolder",position+"");
        JobPagerAdapter adapter = new JobPagerAdapter(fragmentManager,job,jobListFragment,position);
        holder.viewPager.setAdapter(adapter);
        holder.viewPager.setCurrentItem(1);

    }

    public int getItemCount() {
        return mJobList.size();
    }

    public void deleteItem(int index) {
        mJobList.remove(index);
        notifyItemRemoved(index);
        notifyItemRangeChanged(index,mJobList.size());

    }
    //todo: ask shy if this method is doing anything
//    @Override
//    public void onViewRecycled(JobHolder holder) {
//        //super.onViewRecycled(holder);
//        holder.viewPager.setAdapter(null);
//    }

    public class JobHolder extends RecyclerView.ViewHolder{
        ViewPager viewPager;
        public JobHolder(View itemView) {
            super(itemView);
            viewPager=(ViewPager)itemView.findViewById(R.id.slidesPager);
            viewPager.setId(View.generateViewId());
            Log.i("tom vp",viewPager.getId()+"");
        }


    }
}
