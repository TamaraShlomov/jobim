package il.co.hyperactive.jobim.Activitys;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import il.co.hyperactive.jobim.R;

public class FiltetQuestionActivity extends AppCompatActivity implements View.OnClickListener{
    EditText et_question;
    Button bt_yes,bt_no;
    android.support.v7.widget.Toolbar myToolbar;
    String yes_no;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtet_question);
        et_question=(EditText)findViewById(R.id.activity_filtet_et_question);
        bt_yes=(Button) findViewById(R.id.activity_filtet_question_bt_yes);
        bt_no=(Button) findViewById(R.id.activity_filtet_question_bt_no);
        bt_yes.setOnClickListener(this);
        bt_no.setOnClickListener(this);
        initToolBar();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activity_filtet_question_bt_yes:
                bt_yes.setBackgroundResource(R.drawable.item_orange);
                bt_no.setBackgroundResource(R.drawable.item_decorator);
                bt_yes.setTextColor(getResources().getColor(android.R.color.white));
                bt_no.setTextColor(getResources().getColor(android.R.color.black));
                yes_no="yes";
                break;
            case R.id.activity_filtet_question_bt_no:
                bt_yes.setBackgroundResource(R.drawable.item_decorator);
                bt_no.setBackgroundResource(R.drawable.item_orange);
                bt_no.setTextColor(getResources().getColor(android.R.color.white));
                bt_yes.setTextColor(getResources().getColor(android.R.color.black));
                yes_no="no";
                break;
        }
    }
    private void initToolBar() {
        myToolbar=(android.support.v7.widget.Toolbar)findViewById(R.id.toolbar);
        TextView tv_left_action=(TextView)findViewById(R.id.tv_action_left);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        tv_left_action.setText(R.string.add);
        tv_left_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_question.getText().toString().equals("")){
                    et_question.setBackgroundResource(R.drawable.edit_text_user_input_error);
                    Toast.makeText(getBaseContext(),"אנא מלא את שאלתך",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(yes_no==null){
                    Toast.makeText(getBaseContext(),"אנא בחר תשובה",Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent returnIntent = new Intent();
                Bundle result=new Bundle();
                result.putCharSequence("q",et_question.getText().toString());
                result.putCharSequence("answer",yes_no);
                returnIntent.putExtra("result",result);
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }
        });
        TextView toolbar_center=((TextView)myToolbar.findViewById(R.id.tv_toolbar_center));
        toolbar_center.setText(R.string.filter_question);
        toolbar_center.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem backItem=menu.findItem(R.id.menu_item_back);
        backItem.setVisible(true);
        backItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                finish();
                return false;
            }
        });
        return true;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.right_side_toolbar, menu);
        return true;

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
