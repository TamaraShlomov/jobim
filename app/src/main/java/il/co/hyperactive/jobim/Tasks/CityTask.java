package il.co.hyperactive.jobim.Tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;

import org.json.JSONObject;

import java.util.List;

import il.co.hyperactive.jobim.Fragments.registration.CityFragment;
import il.co.hyperactive.jobim.Model.DownloadUrl;
import il.co.hyperactive.jobim.Model.CityJSONParser;

/**
 * Created by Tom on 09/05/2017.
 */

public class CityTask extends AsyncTask<String,Void,List<String>>{
    // Fetches all places from GooglePlaces AutoComplete Web Service
    Context context;
    final String KEY="AIzaSyDxyRcRSVRVWVxSy3oZWuxm4hhksshRkYY";
    public CityTask(Context context) {
        this.context = context;
    }
        @Override
        protected List<String> doInBackground(String... place) {
            // For storing data from web service
            String response = "";
            String key ="key="+KEY;
            String input="input="+place[0];
            // place type to be searched
            String types = "types=(cities)";
            String components="components=country:il";
            String language="language=iw";
            // Building the parameters to the web service
            String parameters = input+"&"+types+"&"+components+"&"+language+"&"+key;
            // Output format
            String output= "json";
            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/"+output+"?"+parameters;
            Log.i("tom","url : "+url);
            List<String> result=null;
            try{
                // Fetching the data from web service in background
                response = DownloadUrl.getResponse(url);
                result=parserJson(response);
            }catch(Exception e){
                Log.e("Background Task",e.toString());
            }
            return result;
        }

    @Override
        protected void onPostExecute(List<String> result) {
        super.onPostExecute(result);
        //String[] from = new String[] {"description"};
       //  int[] to = new int[] { android.R.id.text1 };
        // Creating a SimpleAdapter for the AutoCompleteTextView
       // SimpleAdapter adapter = new SimpleAdapter(context, result, android.R.layout.simple_list_item_1, from, to);
        // Setting the adapter
        ArrayAdapter adapter = new ArrayAdapter<>(context,android.R.layout.simple_list_item_1,result);
        CityFragment.atvPlaces.invalidate();
        CityFragment.atvPlaces.setAdapter(adapter);
        }


    private List<String> parserJson(String responce){
        List<String> places = null;
        CityJSONParser placeJsonParser = new CityJSONParser();
        JSONObject jObject;
        try{
            jObject = new JSONObject(responce);
            // Getting the parsed data as a List construct
            places = placeJsonParser.parse(jObject);

        }catch(Exception e){
            Log.d("Exception",e.toString());
        }
        return places;
    }

}



