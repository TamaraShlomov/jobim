package il.co.hyperactive.jobim.Activitys;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import il.co.hyperactive.jobim.Fragments.findJobs.JobItemIncapsulatedFragment;
import il.co.hyperactive.jobim.Fragments.findJobs.JobListFragment;
import il.co.hyperactive.jobim.Fragments.other.AboutUsFragment;
import il.co.hyperactive.jobim.Fragments.other.NotificationFragment;
import il.co.hyperactive.jobim.Fragments.other.SmartAgentFragment;
import il.co.hyperactive.jobim.Fragments.findJobs.FindJobsFragment;
import il.co.hyperactive.jobim.Fragments.myJobs.MyJobsFragment;
import il.co.hyperactive.jobim.Fragments.searchJobs.SearchFiltersFragment;
import il.co.hyperactive.jobim.Fragments.registration.MyDetailsFragment;
import il.co.hyperactive.jobim.Interface.Filter;
import il.co.hyperactive.jobim.Interface.delete;
import il.co.hyperactive.jobim.Model.SharedPreferencesSingleton;
import il.co.hyperactive.jobim.Model.User;
import il.co.hyperactive.jobim.R;

/**
 * Created by Tom on 22/05/2017.
 */
public class NavigationActivity  extends AppCompatActivity implements View.OnClickListener ,Filter,delete {
    Toolbar myToolbar;
    TextView tv_left_action;
    public Fragment mFragment;
    CircleImageView nav_profil_image;
    User user;
    TextView user_tv_name_last_name;
    TextView center_toolbar;
    ImageView iv_left_action_toolbar;
    View transperentview;
    TextView nav_myDetails,nav_alert,nav_myJobs,nav_findJobs,nav_smartAgent,nav_moreOfUs,nav_publishNewJob;
    boolean openMap=false;
    int currentFragmentId;
    Fragment[] fragmentList;
    boolean onDrawaerCloseBySelectingItemFromOnClick;
    public final static String TAG_FRAGMENT = "Find Job Fragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation);

        fragmentList=new Fragment[]{new MyDetailsFragment(),new NotificationFragment(),new MyJobsFragment(),
                FindJobsFragment.newInstance(false),new SmartAgentFragment(),
                new AboutUsFragment()};

        initNavigation();
        initToolbar();

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, myToolbar, R.string.navigation_drawer_open,R.string.navigation_drawer_close){
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (onDrawaerCloseBySelectingItemFromOnClick){
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                    .replace(R.id.app_bar_fragment_continer, mFragment).commit();
                    onDrawaerCloseBySelectingItemFromOnClick=false;
                }
            }
        };

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //After instantiating your ActionBarDrawerToggle
        toggle.setDrawerIndicatorEnabled(false);
       // Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.nav, getTheme());
        Drawable drawable=getResources().getDrawable(R.drawable.ic_nav);
        drawable=resize(drawable);
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    onDrawaerCloseBySelectingItemFromOnClick=false;
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        chooseFragment();
        if(mFragment instanceof FindJobsFragment){
            //todo: check
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.app_bar_fragment_continer, mFragment,TAG_FRAGMENT).
                    addToBackStack(null).commit();
        }
        else {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.app_bar_fragment_continer, mFragment).commit();
        }
    }

    private Drawable resize(Drawable image) {
        Bitmap b = ((BitmapDrawable)image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, 130, 130, false);
        return new BitmapDrawable(getResources(), bitmapResized);
    }

    private void chooseFragment() {
        String choosenFragment=getIntent().getStringExtra("fragment");
        if(choosenFragment!=null && choosenFragment.equals("MyJobsFragment")) {
            mFragment = MyJobsFragment.newInstance(R.id.fragment_my_jobs_btn_published);
            currentFragmentId=R.id.nav_myJobs;
            changeToolBar(currentFragmentId);
        }
        else {
           // mFragment = FindJobsFragment.newInstance(false, this);
            mFragment=fragmentList[3];
            currentFragmentId=R.id.nav_findJobs;
            changeToolBar(currentFragmentId);
        }
    }

    private void initToolbar() {
        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        center_toolbar=(TextView) myToolbar.findViewById(R.id.tv_toolbar_center);
        center_toolbar.setVisibility(View.VISIBLE);
        tv_left_action=(TextView)myToolbar.findViewById(R.id.tv_action_left);
        tv_left_action.setVisibility(View.GONE);
        iv_left_action_toolbar=(ImageView)findViewById(R.id.imageview_toolbar_left);
        iv_left_action_toolbar.setOnClickListener(this);
        tv_left_action.setOnClickListener(this);
    }

    public void changeToolBar(int id){
        switch (id){
            case R.id.nav_imageView:
            case R.id.nav_myDetails:
                tv_left_action.setText(R.string.setting);
                tv_left_action.setVisibility(View.VISIBLE);
                iv_left_action_toolbar.setVisibility(View.GONE);
                center_toolbar.setText(R.string.MyDeatils);
                break;
            case R.id.nav_alerts:
                tv_left_action.setVisibility(View.GONE);
                center_toolbar.setText(R.string.notification);
                iv_left_action_toolbar.setVisibility(View.GONE);
                break;
            case R.id.nav_myJobs:
                tv_left_action.setVisibility(View.GONE);
                center_toolbar.setText(R.string.myJobs);
                iv_left_action_toolbar.setVisibility(View.GONE);
                break;
            case R.id.nav_findJobs:
                tv_left_action.setVisibility(View.GONE);
                center_toolbar.setText(R.string.jobim);
                iv_left_action_toolbar.setVisibility(View.VISIBLE);
                break;
            case R.id.nav_smartAgent:
                tv_left_action.setVisibility(View.GONE);
                center_toolbar.setText(R.string.smartAgent);
                iv_left_action_toolbar.setVisibility(View.GONE);
                break;
            case R.id.nav_moreOfUs:
                tv_left_action.setVisibility(View.GONE);
                center_toolbar.setText(R.string.about);
                iv_left_action_toolbar.setVisibility(View.GONE);
                break;
        }
    }

    private void initNavigation() {

         nav_profil_image = (CircleImageView) findViewById(R.id.nav_imageView);
         user_tv_name_last_name=(TextView)findViewById(R.id.my_navigation_view_user_name_lastname);
         nav_myDetails=(TextView) findViewById(R.id.nav_myDetails);
         nav_alert=(TextView) findViewById(R.id.nav_alerts);
         nav_myJobs=(TextView) findViewById(R.id.nav_myJobs);

         nav_findJobs=(TextView) findViewById(R.id.nav_findJobs);
         nav_smartAgent=(TextView) findViewById(R.id.nav_smartAgent);
         nav_moreOfUs=(TextView)findViewById(R.id.nav_moreOfUs);
         nav_publishNewJob=(TextView)findViewById(R.id.nav_publishNewJob);

        resizeAllTvIcons();

        transperentview=findViewById(R.id.transperentView);

        nav_profil_image.setOnClickListener(this);
        nav_myDetails.setOnClickListener(this);
        nav_alert.setOnClickListener(this);
        nav_myJobs.setOnClickListener(this);
        nav_findJobs.setOnClickListener(this);
        nav_smartAgent.setOnClickListener(this);
        nav_moreOfUs.setOnClickListener(this);
        nav_publishNewJob.setOnClickListener(this);

    }

    private void resizeAllTvIcons() {
        Drawable image,imageResize;
        image=getResources().getDrawable(R.drawable.ic_my_details);
        imageResize=resize(image);
        nav_myDetails.setCompoundDrawablesRelativeWithIntrinsicBounds(imageResize,null,null,null);

        image=getResources().getDrawable(R.drawable.ic_notifications);
        imageResize=resize(image);
        nav_alert.setCompoundDrawablesRelativeWithIntrinsicBounds(imageResize,null,null,null);

        image=getResources().getDrawable(R.drawable.ic_my_jobs);
        imageResize=resize(image);
        nav_myJobs.setCompoundDrawablesRelativeWithIntrinsicBounds(imageResize,null,null,null);

        image=getResources().getDrawable(R.drawable.ic_find_jobs);
        imageResize=resize(image);
        nav_findJobs.setCompoundDrawablesRelativeWithIntrinsicBounds(imageResize,null,null,null);

        image=getResources().getDrawable(R.drawable.ic_smart_agent);
        imageResize=resize(image);
        nav_smartAgent.setCompoundDrawablesRelativeWithIntrinsicBounds(imageResize,null,null,null);

        image=getResources().getDrawable(R.drawable.ic_about_us);
        imageResize=resize(image);
        nav_moreOfUs.setCompoundDrawablesRelativeWithIntrinsicBounds(imageResize,null,null,null);

        image=getResources().getDrawable(R.drawable.ic_publish_job);
        imageResize=resize(image);
        nav_publishNewJob.setCompoundDrawablesRelativeWithIntrinsicBounds(imageResize,null,null,null);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.nav_imageView:
            case R.id.nav_myDetails:
                mFragment=fragmentList[0];
                break;
            case R.id.nav_alerts:
                mFragment=fragmentList[1];
                break;
            case R.id.nav_myJobs:
                mFragment=fragmentList[2];
                break;
            case R.id.nav_findJobs:
                mFragment=fragmentList[3];
                break;
            case R.id.nav_smartAgent:
                mFragment=fragmentList[4];
                break;
            case R.id.nav_moreOfUs:
                mFragment=fragmentList[5];
                break;
            case R.id.nav_publishNewJob:
               startActivity(new Intent(this,PublishNewJobActivity.class));
                return;
            case R.id.imageview_toolbar_left:
                openMap=!openMap;
                if(openMap)
                    iv_left_action_toolbar.setImageResource(R.drawable.map_icon);
                else
                    iv_left_action_toolbar.setImageResource(R.drawable.ic_location_on_white_24dp);
                mFragment=FindJobsFragment.newInstance(openMap);
                break;
            case R.id.tv_action_left:
                startActivity(new Intent(this,SettingActivity.class));
                break;

        }
        if(v.getId()!=R.id.imageview_toolbar_left &&v.getId()!=R.id.tv_action_left) {
            //onBackPressed();
            onDrawaerCloseBySelectingItemFromOnClick=true;
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            changeToolBar(v.getId());
        }
        else {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.app_bar_fragment_continer, mFragment).commit();
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
          //  final FindJobsFragment fragment = (FindJobsFragment) getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT);
            getSupportFragmentManager().popBackStack();
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        user= SharedPreferencesSingleton.getInstance(this).readUser();
        if(user!=null) {
            Log.e("read user", user.toString());
            user_tv_name_last_name.setText(user.name + " " + user.lastname);
            if (!user.photoPath.equals("empty")) {
                this.nav_profil_image.setImageURI(null);
                this.nav_profil_image.setImageURI(Uri.parse(user.photoPath));
            }
        }
    }

    @Override
    public void showSearchFilters() {

        transperentview.setVisibility(View.VISIBLE);
        transperentview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true; //for cancel the clickable option
            }
        });

        mFragment=new SearchFiltersFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.app_bar_fragment_continer, mFragment).commit();

    }

    @Override
    public void returnToFindJobsFragment() {
        findViewById(R.id.transperentView).setVisibility(View.GONE);
    }

    public void logOut(View view) {
        Intent intent=new Intent(NavigationActivity.this,TokenActivity.class);
        startActivity(intent);
    }

    public void goToSmartAgent(){
        mFragment=fragmentList[4];
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.app_bar_fragment_continer, mFragment).commit();
    }

    @Override
    public void deleteJobFromList(int position) {
        //todo: is this the right way - ask shay
        JobListFragment jobListFragment = (JobListFragment)
                getSupportFragmentManager().findFragmentById(R.id.continer);
        if (jobListFragment != null) {
            jobListFragment.deleteJobFromList(position);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == JobItemIncapsulatedFragment.PICK_JOB_TO_DELETE_REQUEST) {
            if (resultCode ==Activity.RESULT_OK ) {
                deleteJobFromList(data.getIntExtra("position",-1));
            }
        }
    }
}
