package il.co.hyperactive.jobim.Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CityJSONParser {

    /** Receives a JSONObject and returns a list */
    public List<String> parse(JSONObject jObject){

        JSONArray jPlaces = null;
        try {
            /** Retrieves all the elements in the 'places' array */
            jPlaces = jObject.getJSONArray("predictions");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        /** Invoking getPlaces with the array of json object
         * where each json object represent a place
         */
        return getPlaces(jPlaces);
    }

    private List<String> getPlaces(JSONArray jPlaces){
        int placesCount = jPlaces.length();
        List<String> placesList = new ArrayList<>();
      //  HashMap<String, String> place = null;
        String city="";
        /** Taking each place, parses and adds to list object */
        for(int i=0; i<placesCount;i++){
            try {
                /** Call getPlace with place JSON object to parse the place */
                city = getPlace((JSONObject)jPlaces.get(i));
                placesList.add(city);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return placesList;
    }

    /** Parsing the Place JSON object */
    private String getPlace(JSONObject jPlace){

        HashMap<String, String> place = new HashMap<String, String>();

     //   String id="";
       // String reference="";
        String description="";
        String city="";
        try {

            description = jPlace.getString("description");
            city=description.split(",")[0];
          //  id = jPlace.getString("id");
           // reference = jPlace.getString("reference");

         //   place.put("description", description);
        //    place.put("_id",id);
         //   place.put("reference",reference);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return city;
    }
}
