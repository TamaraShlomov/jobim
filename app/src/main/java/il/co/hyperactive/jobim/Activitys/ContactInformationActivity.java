package il.co.hyperactive.jobim.Activitys;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import il.co.hyperactive.jobim.Model.FireBaseSingleton;
import il.co.hyperactive.jobim.Model.Job;
import il.co.hyperactive.jobim.Model.SharedPreferencesSingleton;
import il.co.hyperactive.jobim.R;

public class ContactInformationActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    Toolbar myToolbar;
    TextView tv_left_action, tv_title;
    EditText et_email, et_sms, et_call;
    CheckBox checkBox_sms, checkBox_phone, checkBox_email;
    View view_sms, view_phone, view_email, view_enter_phone_number;
    ImageView imageView_sms, imageView_phone, imageView_email;

    String phone_number_for_sms, phone_number_for_call, mail;
    Job job;
    SharedPreferencesSingleton sharedPreferencesSingleton;
    FireBaseSingleton fireBaseSingleton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferencesSingleton = SharedPreferencesSingleton.getInstance(getBaseContext());
        fireBaseSingleton = FireBaseSingleton.getInstance(this);
        setContentView(R.layout.activity_contact_information);
        initToolbar();
        initViews();
        job = (Job) getIntent().getSerializableExtra("job");

        phone_number_for_sms = phone_number_for_call = sharedPreferencesSingleton.readPhoneNumber();
        mail = sharedPreferencesSingleton.readUser().email;
        et_sms.setText(phone_number_for_sms);
        et_call.setText(phone_number_for_call);
        et_email.setText(mail);
    }

    private void initViews() {
        checkBox_sms = (CheckBox) findViewById(R.id.activity_contact_information_checkBox_sms);
        checkBox_phone = (CheckBox) findViewById(R.id.activity_contact_information_checkBox_phone);
        checkBox_email = (CheckBox) findViewById(R.id.activity_contact_information_checkbox_mail);
        checkBox_sms.setOnCheckedChangeListener(this);
        checkBox_phone.setOnCheckedChangeListener(this);
        checkBox_email.setOnCheckedChangeListener(this);

        view_sms = findViewById(R.id.activity_contact_information_layout_sms);
        view_phone = findViewById(R.id.activity_contact_information_layout_phone);
        view_email = findViewById(R.id.activity_contact_information_layout_mail);
        view_enter_phone_number = findViewById(R.id.activity_contact_information_layout_enter_phone);

        view_sms.setOnClickListener(this);
        view_phone.setOnClickListener(this);
        view_email.setOnClickListener(this);
        view_enter_phone_number.setOnClickListener(this);

        imageView_sms = (ImageView) findViewById(R.id.imageView_message_sms);
        imageView_phone = (ImageView) findViewById(R.id.imageView_phone);
        imageView_email = (ImageView) findViewById(R.id.imageView_mail);

        et_email = (EditText) findViewById(R.id.activity_contact_information_et_email);
        et_sms = (EditText) findViewById(R.id.et_phoneNumber_sms);
        et_call = (EditText) findViewById(R.id.et_phoneNumber_call);

        tv_title = (TextView) findViewById(R.id.activity_contact_information_textview_title);
    }

    private void initToolbar() {
        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        tv_left_action = (TextView) myToolbar.findViewById(R.id.tv_action_left);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        tv_left_action.setText(R.string.publish);
        tv_left_action.setOnClickListener(this);
        TextView center = (TextView) myToolbar.findViewById(R.id.tv_toolbar_center);
        center.setVisibility(View.VISIBLE);
        center.setText(R.string.contact_us);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.right_side_toolbar, menu);
        MenuItem exitItem = menu.findItem(R.id.menu_item_back);
        exitItem.setVisible(true);
        exitItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                finish();
                return false;
            }
        });

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_back:
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_action_left:
                if (!checkBox_sms.isChecked() && !checkBox_phone.isChecked() && !checkBox_email.isChecked()) {
                    Toast.makeText(getBaseContext(), "יש למלא אמצעי יצירת קשר תקינים", Toast.LENGTH_SHORT).show();
                    break;
                }
                phone_number_for_sms = phone_number_for_call = mail = "";
                if (checkBox_sms.isChecked())
                    phone_number_for_sms = et_sms.getText().toString();
                if (checkBox_phone.isChecked())
                    phone_number_for_call = et_call.getText().toString();
                if (checkBox_email.isChecked())
                    mail = et_email.getText().toString();

                job.setSms(phone_number_for_sms);
                job.setCall(phone_number_for_call);
                job.setMail(mail);

                fireBaseSingleton.WriteNewJob(job,sharedPreferencesSingleton.readPhoneNumber(),this);
                Intent intent=new Intent(ContactInformationActivity.this,NavigationActivity.class);
                intent.putExtra("fragment","MyJobsFragment");
                startActivity(intent);
                Toast.makeText(getBaseContext(),"done",Toast.LENGTH_SHORT).show();

                break;
            case R.id.activity_contact_information_layout_sms:
                tv_title.setText(R.string.message_sent_to_number);
                view_enter_phone_number.setVisibility(View.VISIBLE);
                et_sms.setVisibility(View.VISIBLE);
                et_call.setVisibility(View.GONE);
                et_email.setVisibility(View.GONE);
                break;
            case R.id.activity_contact_information_layout_phone:
                tv_title.setText(R.string.call_forward_to_number);
                view_enter_phone_number.setVisibility(View.VISIBLE);
                et_call.setVisibility(View.VISIBLE);
                et_sms.setVisibility(View.GONE);
                et_email.setVisibility(View.GONE);
                break;
            case R.id.activity_contact_information_layout_mail:
                tv_title.setText(R.string.mail_sent_to_address);
                view_enter_phone_number.setVisibility(View.GONE);
                et_email.setVisibility(View.VISIBLE);

                break;
        }
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
           switch (buttonView.getId()){
               case R.id.activity_contact_information_checkBox_sms:
                   if(isChecked)
                       imageView_sms.setImageResource(R.drawable.sms);
                   else
                       imageView_sms.setImageResource(R.drawable.sms_disabled);
                   break;
               case R.id.activity_contact_information_checkBox_phone:
                   if(isChecked)
                        imageView_phone.setImageResource(R.drawable.call);
                   else
                       imageView_phone.setImageResource(R.drawable.call_disabled);
                   break;
               case R.id.activity_contact_information_checkbox_mail:
                   if(isChecked)
                    imageView_email.setImageResource(R.drawable.email);
                   else
                       imageView_email.setImageResource(R.drawable.email_diabled);
                   break;
           }
    }
}
