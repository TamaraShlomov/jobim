package il.co.hyperactive.jobim.Model;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tom on 15/05/2017.
 */

public class User implements Serializable {
    public String name;
    public String lastname;
    public String city;
    public String year;
    public String email;
    public String photoPath;
    //public String phoneNumber;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String name, String lastname,String city,String year,String email,String photoPath) {
      //  this.phoneNumber=phoneNumber;
        this.name = name;
        this.email = email;
        this.year = year;
        this.city = city;
        this.lastname = lastname;
        this.photoPath=photoPath;
    }

    //check if this method necessary
    @Exclude
    public Map<String, String> toMap() {
        HashMap<String, String> result = new HashMap<>();
      //  result.put("uid", phoneNumber);
        result.put("name", name);
        result.put("lastname", lastname);
        result.put("city", city);
        result.put("year", year);
        result.put("email", email);
        result.put("photoPath",photoPath);

        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", city='" + city + '\'' +
                ", year='" + year + '\'' +
                ", email='" + email + '\'' +
                ", photoPath='" + photoPath + '\'' +
                '}';
    }
}
